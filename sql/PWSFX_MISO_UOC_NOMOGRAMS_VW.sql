CREATE OR REPLACE VIEW PWSFX_MISO_UOC_NOMOGRAMS_VW comment = 'MISO UOC NOMOGRAMS PART' AS
SELECT CID,
CONCAT(IFNULL(CID,0),' || ',UPPER(CONSTRAINT_NAME),' || ',UPPER(CONTINGENCY)) AS FULL_INTERFACE_NAME, 
UPPER(CONSTRAINT_NAME) AS CONSTRAINT_NAME, UPPER(CONTINGENCY) AS CONTINGENCY_UID, DEVICENAME AS MONEL_MEMO, CONVENTION
FROM POWERDB_BIZDEV.PTEST.PWSFX_MISO_NMGRMS 
WHERE CID IS NOT NULL AND CLASS  = 'Peak'