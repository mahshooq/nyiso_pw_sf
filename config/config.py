# -*- coding: utf-8 -*-
"""
Created on Wed Aug  14 12:45:12 2021

@author: Maxim.Naglis
"""

import sys
from configparser import ConfigParser

sys.path.append('C:\\Users\\MAHSHOOQ.MAJEED\\OneDrive - BioUrja Trading LLC\\Documents\\PWSFX\\NYISO_PWSFX_TEST\\pwsfx\\utils')
from utils import makehash
import logging

def getSettings(configFile = None):

    parser = ConfigParser()

    if configFile:
        parser.read(configFile)
    else:
        import os
        homePath = os.environ['USERPROFILE']
        configFile = "\\".join([homePath,'.pwsfx_app.cfg'])
        #print(configFile)
        parser.read(configFile)
        
    connData = makehash()
    
    #print(parser.sections())

    for sectionName in parser.sections():
        logging.debug("-"*60)
        logging.debug('Settings: ',sectionName)
        for name, value in parser.items(sectionName):
            logging.debug(' %s = %s' % (name,value))
            connData[sectionName][name] = value
    logging.debug("-"*60)
    return connData

def getConnData(configFile = None):

    parser = ConfigParser()
    
    if configFile:
        parser.read(configFile)
    else:
        import os
        homePath = os.environ['USERPROFILE']
        configFile = "\\".join([homePath,'.pwsfx_db.cfg'])
        #print(configFile)
        parser.read(configFile)
        
    connData = makehash()

    for sectionName in parser.sections():
        logging.debug("-"*60)
        logging.debug('Database: ',sectionName)
        for name, value in parser.items(sectionName):
            logging.debug(' %s = %s' % (name,value))
            connData[sectionName][name] = value
    logging.debug("-"*60)
    return connData

connData = getConnData()
globalSettings = getSettings()

if __name__ == '__main__':

    pass
