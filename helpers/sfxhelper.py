import sys
import os
import numpy as np
import subprocess
import argparse
import shutil
import glob
import decimal
import datetime
from time import strftime,localtime
from retry import retry
import multiprocessing
import pandas as pd
pd.options.display.max_columns = None

from pwsfx.exceptions.exceptions import *
from pwsfx.config.config import connData as cd
from pwsfx.db.db import create_snowflake_writer_engine
from pwsfx.db.db import create_snowflake_reader_engine
from pwsfx.db.db import get_writer_conn
from pwsfx.db.db import execute_query
from pwsfx.db.db import deleteSFXfromSF, saveSFXtoSF
from pwsfx.helpers.auxhelper import fullPath, add_runid
from pwsfx.helpers.outagehelper import generateDateList
from pwsfx.db.bulk_upload import csv_to_table_bulk_upload
from pwsfx.db.bulk_upload import pandas_to_table_bulk_upload

from pwsfx.config.config import globalSettings as gs

def convertTLRtoSFX(isoStr,outputDir,fileName, run_type='FWD'):

    fileNameFullPath = "\\".join([outputDir,fileName])
    fileNameFullPath = fileNameFullPath.replace('\\','//')
    fileNameFullPath = fileNameFullPath.replace('///','//')
    fileNameFullPath = fileNameFullPath.replace('//','\\')
    df = pd.read_csv(fileNameFullPath,header = 1)

    df.drop('ETLR',axis=1,inplace = True)
    df.drop('WTLR',axis=1,inplace = True)

    df.columns =['Name'] +  [x.split(' ',1)[1].replace('(','').replace(')','') for x in df.columns[1:]]

    sfx_dictlist = []
    df_dictlist = df.to_dict('records')
    for x in df_dictlist:
        node = x['Name']
        for key in [key for key in x.keys() if key != 'Name']:
            cnx = key
            sfx = x[key]
            cid = key.split('||',1)[0].strip()
            ctg = key.split('||')[-1].strip()
            #sfx_dictlist.append({'BU_ISO':isoStr,'CNX':cnx,'CID':cid,'CTG':ctg,'NODE':node,'SFX':sfx})
            sfx_dictlist.append({'BU_ISO':isoStr,'CNX':cnx,'CID':cid,'CTG':ctg,'NODE':node,'SFX':sfx})

    #sfx_df = pd.DataFrame(data=sfx_dictlist,columns=['BU_ISO','CNX','CID','CTG','NODE','SFX'])
    sfx_df = pd.DataFrame(data=sfx_dictlist,columns=['BU_ISO','CNX','CID','NODE','SFX'])
    #Need to change SFX sign
    sfx_df['SFX'] = sfx_df['SFX']*(-1.0)
    sfx_df['CID'] = sfx_df['CID'].astype('int64')
    #sfx_df['RUN_TYPE'] = run_type
    sfx_df[(sfx_df['NODE']!='{0}.TOTAL.LOAD'.format(isoStr))]
    
    return sfx_df

def getBUCIDtoCNXmap(startDate, endDate, isoStr = 'MISO', dbstr = 'snowflakedb'):

    sqlStrTmp = '''       
    SELECT BU_CID, CID, FULL_INTERFACE_NAME AS CNX
    FROM POWERDB_BIZDEV.PTEST.PWSFX_UOC
    WHERE BU_ISO = '{isoStr}' AND FLOW_STARTDATE = '{startDate}' AND FLOW_ENDDATE  = '{endDate}' 
    AND INSERT_DATE = (SELECT MAX(INSERT_DATE) FROM POWERDB_BIZDEV.PTEST.PWSFX_UOC 
    WHERE BU_ISO = '{isoStr}' AND FLOW_STARTDATE = '{startDate}' AND FLOW_ENDDATE  = '{endDate}')
    '''
    sqlStr =  sqlStrTmp.format(startDate=startDate,endDate=endDate,isoStr=isoStr)
    
    df = None
    db_engine = create_snowflake_reader_engine(account=cd[dbstr]['account'],user=cd[dbstr]['user'],
                                               password=cd[dbstr]['password'],role=cd[dbstr]['role'],
                                               warehouse=cd[dbstr]['warehouse'],database=cd[dbstr]['database'])
    con = db_engine.connect()

    try:
        df = pd.read_sql(sqlStr, con = con, index_col = None)
    except:
        import sys, traceback
        exc_type, exc_value, exc_traceback = sys.exc_info()
        print("-"*60)
        print("*** print_tb:")
        traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
        print("*** print_exception:")
        traceback.print_exception(exc_type, exc_value, exc_traceback,
                                  limit=2, file=sys.stdout, chain=True)
        print("-"*60)
        raise
    finally:
        con.close()
        db_engine.dispose()

    df.columns = [x.upper() for x in df.columns]
    df['CID'] = df['CID'].astype('int64')
    df['BU_CID'] = df['BU_CID'].astype('int64')
    return df    

def writeSFXtoCSV_worker(args):
    
    isoStr, outputDir, runid, flowmonth, cnxmap = args
    
    filename = add_runid('multbustlrs.csv',runid)
    df = convertTLRtoSFX(isoStr=isoStr,outputDir=outputDir,fileName=filename)
    df['RUNID'] = runid
    df['FLOWMONTH'] = flowmonth
    df = df.merge(cnxmap[['BU_CID','CNX']],how='inner',left_on=['CNX'],right_on=['CNX'])
    sfx_filename = outputDir+'//'+add_runid('sfxmatrix.csv',runid)

    df[['BU_ISO','BU_CID','CID','NODE','SFX','RUNID','FLOWMONTH']].to_csv(sfx_filename,index=False)
    #sfx_df = df[['BU_ISO','BU_CID','CID','NODE','SFX','RUNID','FLOWMONTH']]

    return runid, sfx_filename

@retry(Error, tries=3, delay=3, backoff=2)
def uploadSFXCSVtoSF_worker(args):

    try:
        isoStr, outputDir, runid, flowmonth, cnxmap = args
        sfx_filename = outputDir+'//'+add_runid('sfxmatrix.csv',runid)


        dbstr = 'snowflakedb_write'    
        db_conn = get_writer_conn(account = cd[dbstr]['account'],user=cd[dbstr]['user'],password=cd[dbstr]['password'],role=cd[dbstr]['role'],warehouse=cd[dbstr]['warehouse'],database=cd[dbstr]['database'],schema=cd[dbstr]['schema'])
        
        deleteSFXfromSF(db_conn = db_conn, isoStr = isoStr, startDate = flowmonth, endDate = None, runId = runid, tableName = gs[isoStr]['tablename_daily'])
    
        done = csv_to_table_bulk_upload(csv_file_path = sfx_filename,
                                        table_name = gs[isoStr]['tablename_daily'].split('.')[-1],
                                        account_name = cd[dbstr]['account'],
                                        schema_name = cd[dbstr]['schema'],
                                        database_name = cd[dbstr]['database'],
                                        warehouse_name = cd[dbstr]['warehouse'],
                                        username = cd[dbstr]['user'],
                                        password = cd[dbstr]['password'],
                                        if_exists = 'append')
        if not done:
            raise Error
    except:
        raise Error

    
    return runid, done
    

if __name__ == '__main__':

    start = datetime.datetime.now()

    parser = argparse.ArgumentParser(description='Dumper.')
    parser.add_argument('--iso', action='store', dest = 'isoStr', type=str, default = 'MISO')
    parser.add_argument('--start', action='store', dest = 'startDate', type=str, default = '2022-02-01')
    parser.add_argument('--end', action='store', dest = 'endDate', type=str, default = '2022-02-28')
    args = parser.parse_args()

    if type(args.startDate) == type(None):
        startdate = gs[args.isoStr]['startdate']
    else:
        startdate = args.startDate
    if type(args.endDate) == type(None):
        enddate = gs[args.isoStr]['enddate']
    else:
        enddate = args.endDate
    
    dbstr = 'snowflakedb_write'    
    outputDir = gs[args.isoStr]['output_dir']
    inputDir = gs[args.isoStr]['input_dir']

    targetdates = generateDateList(startdate = startdate, enddate = enddate)
    runs = ['BASE'] + [x.format('YYYY-MM-DD') for x in targetdates]
    runids = [x for x in enumerate(runs)]

    cnxmap = getBUCIDtoCNXmap(startDate = startdate, endDate=enddate, isoStr = args.isoStr, dbstr = 'snowflakedb')
    
    worker_args = []
    
    for runid in [x[0] for x in runids]:
        worker_args.append((args.isoStr, outputDir, runid, startdate, cnxmap))

    sfx_dict = {}
    if True:
        #pool=multiprocessing.Pool(processes=multiprocessing.cpu_count())
        #I can only allocate 61.5GB of memory on my laptop, so I had to limit number of processes to 3, otherwise memory error
        pool=multiprocessing.Pool(processes=4)
        result=pool.map_async(writeSFXtoCSV_worker,worker_args)
        pool.close()
        pool.join()

        for x in result.get(timeout=1):
            print(x)
            sfx_dict[x[0]] = x[1]

    upload_dict = {}
    if False:
        #pool=multiprocessing.Pool(processes=multiprocessing.cpu_count())
        #If more than 4 processes than I get timeouts - limited bandwidth for upload
        pool=multiprocessing.Pool(processes=4)
        result=pool.map_async(uploadSFXCSVtoSF_worker,worker_args)
        pool.close()
        pool.join()

        for x in result.get(timeout=1):
            print(x)
            upload_dict[x[0]] = x[1]

            
    if True:
        for runid in [x[0] for x in runids]:            
            db_conn = get_writer_conn(account = cd[dbstr]['account'],
                                      user=cd[dbstr]['user'],
                                      password=cd[dbstr]['password'],
                                      role=cd[dbstr]['role'],
                                      warehouse=cd[dbstr]['warehouse'],
                                      database=cd[dbstr]['database'],
                                      schema=cd[dbstr]['schema'])
            deleteSFXfromSF(db_conn = db_conn, isoStr = args.isoStr, startDate = startdate, endDate = enddate, runId = runid,
                            tableName = gs[args.isoStr]['tablename_daily'])

            sfx_filename = outputDir+'//'+add_runid('sfxmatrix.csv',runid)
            sfx_dict[runid] = sfx_filename
            done = csv_to_table_bulk_upload(csv_file_path = sfx_dict[runid],
                                            table_name = gs[args.isoStr]['tablename_daily'].split('.')[-1],
                                            account_name = cd[dbstr]['account'],
                                            schema_name = cd[dbstr]['schema'],
                                            database_name = cd[dbstr]['database'],
                                            warehouse_name = cd[dbstr]['warehouse'],
                                            username = cd[dbstr]['user'],
                                            password = cd[dbstr]['password'],
                                            if_exists = 'append')

            
    end = datetime.datetime.now()
    runtime = (end-start).seconds

    print('PWSFX sfx transformation and upload to DB is done. Runtime was {0} seconds'.format(runtime))
