import sys
import os
import numpy as np
import subprocess
import argparse
import shutil
import glob
import decimal
import multiprocessing
import pandas as pd
import datetime
pd.options.display.max_columns = None

sys.path.append('C:\\Users\\MAHSHOOQ.MAJEED\\OneDrive - BioUrja Trading LLC\\Documents\\PWSFX\\NYISO_PWSFX_TEST\\pwsfx\\exceptions')
sys.path.append('C:\\Users\\MAHSHOOQ.MAJEED\\OneDrive - BioUrja Trading LLC\\Documents\\PWSFX\\NYISO_PWSFX_TEST\\pwsfx\\config')
sys.path.append('C:\\Users\\MAHSHOOQ.MAJEED\\OneDrive - BioUrja Trading LLC\\Documents\\PWSFX\\NYISO_PWSFX_TEST\\pwsfx\\db')
sys.path.append('C:\\Users\\MAHSHOOQ.MAJEED\\OneDrive - BioUrja Trading LLC\\Documents\\PWSFX\\NYISO_PWSFX_TEST\\pwsfx\\helpers\\outagehelper')
sys.path.append('C:\\Users\\MAHSHOOQ.MAJEED\\OneDrive - BioUrja Trading LLC\\Documents\\PWSFX\\NYISO_PWSFX_TEST\\pwsfx\\helpers\\auxhelper')
sys.path.append('C:\\Users\\MAHSHOOQ.MAJEED\\OneDrive - BioUrja Trading LLC\\Documents\\PWSFX\\NYISO_PWSFX_TEST\\pwsfx\\helpers\\sfhelper')

from exceptions import *
from config import connData as cd
from config import globalSettings as gs
from db import create_snowflake_writer_engine
from db import create_snowflake_reader_engine

from auxhelper import fullPath, add_runid
from sfhelper import insert_obj_list
from outagehelper import generateDateList

def calctlr_worker(args):

    runid, pwexe, auxFile = args
    start = datetime.datetime.now()
    status = subprocess.check_call([pwexe,auxFile])
    end = datetime.datetime.now()
    runtime = (end-start).seconds
    return (runid,runtime,status)

if __name__ == '__main__':

    start = datetime.datetime.now()
    
    parser = argparse.ArgumentParser(description='Dumper.')
    parser.add_argument('--iso', action='store', dest = 'isoStr', type=str, default = 'NYISO')
    parser.add_argument('--start', action='store', dest = 'startDate', type=str, default = '2022-02-01')
    parser.add_argument('--end', action='store', dest = 'endDate', type=str, default = '2022-02-28')
    args = parser.parse_args()

    if type(args.startDate) == type(None):
        startdate = gs[args.isoStr]['startdate']
    else:
        startdate = args.startDate
    if type(args.endDate) == type(None):
        enddate = gs[args.isoStr]['enddate']
    else:
        enddate = args.endDate
    
    dbstr = 'snowflakedb'
    db_engine = create_snowflake_writer_engine(
        account = cd[dbstr]['account'],
        user=cd[dbstr]['user'],
        password=cd[dbstr]['password'],
        role=cd[dbstr]['role'],
        warehouse=cd[dbstr]['warehouse'],
        database=cd[dbstr]['database'],
        schema=cd[dbstr]['schema'])
    
    outputDir = gs[args.isoStr]['output_dir']
    inputDir = gs[args.isoStr]['input_dir']

    targetdates = generateDateList(startdate = '2022-02-01', enddate = '2022-02-28')
    runs = ['BASE'] + [x.format('YYYY-MM-DD') for x in targetdates]
    runids = [x for x in enumerate(runs)]

    worker_args = []
    for runid in [x[0] for x in runids]:
        calctlrFile = add_runid('calctlrs.aux',runid)
        calctlrFileFullPath = fullPath(outputDir,calctlrFile)
        worker_args.append((runid,gs[args.isoStr]['pwexe'],calctlrFileFullPath))
        
    pool=multiprocessing.Pool(processes=multiprocessing.cpu_count())
    result=pool.map_async(calctlr_worker,worker_args)
    pool.close()
    pool.join()
    
    for x in result.get(timeout=1):
        print(x)
        
    end = datetime.datetime.now()
    runtime = (end-start).seconds

    print('PWSFX calculation is done. Runtime was {0} seconds'.format(runtime))
