import sys
import os
import numpy as np
import subprocess
import argparse
import shutil
import glob
import decimal
import pandas as pd
pd.options.display.max_columns = None

from pwsfx.exceptions.exceptions import *
from pwsfx.config.config import connData as cd
from pwsfx.db.db import create_snowflake_writer_engine

from pwsfx.config.config import globalSettings as gs
from pwsfx.helpers.sfhelper import insert_obj_list
from pwsfx.helpers.sfhelper import MISOInjectionGroups, SPPISOInjectionGroups
from pwsfx.helpers.sfhelper import MISOPowerWorldBuses, SPPISOPowerWorldBuses
from pwsfx.helpers.sfhelper import MISOPowerWorldBranches, SPPISOPowerWorldBranches
from pwsfx.helpers.sfhelper import MISOPowerWorldGens, SPPISOPowerWorldGens
from pwsfx.helpers.sfhelper import MISOPowerWorldLoads, SPPISOPowerWorldLoads
from pwsfx.helpers.sfhelper import MISOMonitoredElements, SPPISOMonitoredElements
from pwsfx.helpers.sfhelper import MISOContingencies, SPPISOContingencies
from pwsfx.helpers.sfhelper import MISOScheduledOutages, SPPISOScheduledOutages
from pwsfx.helpers.sfhelper import MISOSourceSink, SPPISOSourceSink
from pwsfx.helpers.sfhelper import MISOFlowgates, SPPISOFlowgates
from pwsfx.helpers.sfhelper import MISOEMSContingencies
from pwsfx.helpers.sfhelper import MISONomograms, SPPISONomograms

def prepInjectionGroups(df, busdf, isoStr = 'MISO'):

    if isoStr == 'MISO':
        df = df[df['CLASS'] == 'Peak']
        load_df = df[df['TYPE'] == 'Load Zone']
    elif isoStr == 'SPPISO':
        df = df[df['CLASS'] == 'ON']
        load_df = df[df['TYPE'] == 'DND']
        
    srcsnk_dict = df.to_dict(orient='records')
    srcsnkgrp = df.groupby(['PNODENAME'])['PARTFACTOR'].sum()
    srcsnkgrp_dict = srcsnkgrp.to_dict()

    injgroup_list = []

    counter = 0
    for record in srcsnk_dict:
        try:
            object_str = "Bus '{0}'".format(str(record['BUSNUM']))
            parfac = 1000*record['PARTFACTOR']/srcsnkgrp_dict[record['PNODENAME']]
            injpoint = {'ID':counter,'OBJECT':object_str,'CONTAINEDBY':record['PNODENAME'],'PARFAC':decimal.Decimal(parfac).quantize(decimal.Decimal('0.000001'))}
            injgroup_list.append(injpoint)
            counter = counter + 1
        except:
            raise

    loadbus_memos = load_df['BUSNAME'].unique()
    totloadmwh = busdf[busdf['CLEANMEMO'].isin(loadbus_memos)]['LOADMW'].sum()

    busloads = busdf[~busdf['LOADMW'].isna()][['CLEANMEMO','BUSNUM','LOADMW']]
    busloads_dict = busloads.to_dict(orient='records')

    loadinjgroup_list = []
    for record in busloads_dict:
        if record['CLEANMEMO'] in loadbus_memos:
            try:
                object_str = "Bus '{0}'".format(str(record['BUSNUM']))
                #if type(record['LOADMW']) == type(None):
                #    print(record)
                parfac = 1000*record['LOADMW']/totloadmwh
                injpoint = {'ID':counter,'OBJECT':object_str,'CONTAINEDBY':isoStr+'.TOTAL.LOAD','PARFAC':decimal.Decimal(parfac).quantize(decimal.Decimal('0.000001'))}
                loadinjgroup_list.append(injpoint)
                counter = counter + 1
            except:
                raise

    return injgroup_list + loadinjgroup_list             

def prepPWBus(df,filename):

    df = df.rename(columns={'Number':'BUSNUM','Name':'BUSNAME'})
    df['FILENAME'] = filename
    df.columns = [x.upper() for x in df.columns]
    bus_list = df.to_dict(orient='records')
    return bus_list, df
    
def prepPWBranch(df,filename):

    df['FILENAME'] = filename
    df.columns = [x.upper() for x in df.columns]
    branch_list = df.to_dict(orient='records')    
    return branch_list, df

def prepPWGen(df,filename):

    df = df.rename(columns={'ID':'GENID'})
    df['FILENAME'] = filename
    df.columns = [x.upper() for x in df.columns]
    gen_list = df.to_dict(orient='records')    
    return gen_list, df

def prepPWLoad(df,filename):

    df = df.rename(columns={'ID':'LOADID'})
    df['FILENAME'] = filename
    df.columns = [x.upper() for x in df.columns]
    load_list = df.to_dict(orient='records')    
    return load_list, df

def parse_cimctgs_file(filename):

    df = pd.read_csv(filename,header=None,na_filter=False,dtype=str)
    df = df.iloc[7: , :]
    df.rename(columns={1:'CTGID',2:'CTG_FACILITY',5:'EQTYPE',6:'KEY1',7:'KEY2',8:'KEY3',9:'ACTION',10:'IDCMAPPING'},inplace=True)
    df = df[['CTGID','CTG_FACILITY','EQTYPE','KEY1','KEY2','KEY3','ACTION','IDCMAPPING']]
    df = df[df['EQTYPE'].isin(['XF','LN'])]
    df['MEMONOSPACE'] = df.apply(lambda row: (row['KEY1']+row['KEY2']).replace(' ','') if row['EQTYPE'] == 'LN' else (row['KEY1']+row['KEY3']).replace(' ',''),axis=1)
    df['FILENAME'] = os.path.basename(filename)
    df.columns = [x.upper() for x in df.columns]
    record_list = df.to_dict(orient='records')    
    return record_list, df    

#Name	Class	Contingency	Constraint	Limit	Device	Type	Direction	Factor
#1531:TEKAMAH-SUB1216 FLO FORT CALHOUN-RAUN	Peak	MECOPP1	1531:TEKAMAH-SUB1216 FLO FORT CALHOUN-RAUN	32.9	TEKAMSUB1216_1 1	Line	From-To	1
#1532:TEKAMAH-SUB1216 FLO FORT CALHOUN-RAUN	Peak	MECOPP1	1532:TEKAMAH-SUB1216 FLO FORT CALHOUN-RAUN	60	TEKAMSUB1216_1 1	Line	To-From	1
#155114:BCKBONE BCKBODUNDE69_1 1 BASE	Peak		155114:BCKBONE BCKBODUNDE69_1 1 BASE	55.4	BCKBODUNDE69_1 1	Line	From-To	1
#157062:14L TAP-HIBBING FLO 78L TAP	Peak	MP11030	157062:14L TAP-HIBBING FLO 78L TAP	84	14L_THIBBI11_1 1	Line	From-To	1

def parse_file(filename, is_outages = False, if_flowgates = False, if_nomograms = False):

    df = pd.read_csv(filename, header=0)
    if if_nomograms:
        pass
        try:
            df['Contingency'] = df['Contingency'].fillna('BASE')
            #df['Contingency'] = df.apply(lambda row: 'BASE' if row['Contingency'] == np.nan else row['Contingency'],axis = 1)
            df['Convention'] = df.apply(lambda row: (1 if row['Direction'] == 'From-To' else -1),axis=1)    
            transfer = df[df['Type'].isin(['Source','Sink'])]
            df.drop('Name', axis=1, inplace=True)
            df = df[~df['Type'].isin(['Source','Sink'])]
            
            nmgrms = df[df['Constraint'].str.contains(':')]
            nmgrms['cid'] = nmgrms.apply(lambda row: int(str(row['Constraint']).split(':')[0]),axis=1)
            nmgrms['constraint_name'] = nmgrms.apply(lambda row: str(row['Constraint']).split(':')[1].upper(),axis=1)
            nmgrms['FGNAME'] = np.nan
            nmgrms.drop('Constraint', axis=1, inplace=True)
            nmgrms.rename(columns={'Device':'DeviceName','Type':'DeviceType'}, inplace=True)
            nmgrms = nmgrms[['cid', 'constraint_name','FGNAME','DeviceName', 'DeviceType', 'Direction','Convention', 'Limit','Contingency','Class']]
            
            df = df[~df['Constraint'].str.contains(':')]
            flgts = df
            
            flgts['FGNAME'] = flgts['Constraint']
            flgts['FGNAME'] = flgts.apply(lambda row: str(row['FGNAME']).replace(row['Contingency'],''),axis=1) 
            flgts['FGNAME'] = flgts.apply(lambda row: str(row['FGNAME']).split(' ')[0],axis=1)
            #flgts['FGNAME'] = flgts.apply(lambda row: str(row['FGNAME']).replace(row['Device'],''),axis=1)
            flgts['FGNAME'] = flgts.apply(lambda row: row['FGNAME'].strip(),axis=1)
            flgts['FGNAME'] = flgts.apply(lambda row: str(row['FGNAME']).replace('FG',''),axis=1)
            flgts['constraint_name'] = np.nan
            flgts['cid'] = np.nan
            flgts.drop('Constraint', axis=1, inplace=True)
            flgts.rename(columns={'Device':'DeviceName','Type':'DeviceType'}, inplace=True)
            flgts = flgts[['cid', 'constraint_name','FGNAME','DeviceName', 'DeviceType', 'Direction','Convention', 'Limit','Contingency','Class']]
            
            df = pd.concat([flgts,nmgrms])
            
        except:
            print(flgts)
            print(flgts.dtypes)           
            raise
            
    if if_flowgates:
        try:
            df['ContingencyName'] = df.apply(lambda row: 'BASE' if row['Type'] == 'PTDF' else row['ContingencyName'],axis = 1)
            #print(df.head())
        except:
            pass
    if not if_nomograms:
        df = df.fillna(method='ffill')
    if is_outages:
        try:
            df = df.rename(columns={'Outage':'TICKETID'})
        except:
            df = df.rename(columns={'Name':'TICKETID'})   
        
    df['FILENAME'] = os.path.basename(filename)
    df.columns = [x.upper() for x in df.columns]
    df_nonulls = df.astype(object).where(pd.notnull(df),None)
    dict_list = df_nonulls.to_dict(orient='records')
    return dict_list, df_nonulls 

def parsePowerWorldDumpFile(filename, is_gen_or_load = False):

    if is_gen_or_load:
        df = pd.read_csv(filename,header=1,dtype={'ID':str},na_filter=False)
    else:
        df = pd.read_csv(filename,header=1)

    df_nonulls = df.astype(object).where(pd.notnull(df),None)
    df_nonulls.replace(r'^\s+$', None, regex=True)
    try:
        df_nonulls['CleanMemo'] = df_nonulls.apply(lambda row: str(row['Memo'].split(",")[0].replace("/* [","").replace("] */","").strip()),axis=1)
        df_nonulls['CleanMemo'] = df_nonulls.apply(lambda row: str(row['CleanMemo'].split("<")[0].replace("/* [","").replace("] */","").strip()),axis=1)
        df_nonulls['CleanMemo'] = df_nonulls.apply(lambda row: str(row['CleanMemo'].replace("'","").strip()),axis=1)
    except:
        pass
    return df_nonulls 

def parse_srcsnk(filename, busdf):

    df = pd.read_csv(filename,header=0)
    df['FILENAME'] = os.path.basename(filename)
    df = df.fillna(method='ffill')
    df = df.rename(columns={'Name':'PNODENAME'})
    
    if 'participationFactor' not in df.columns:
        df['PARTFACTOR']=1.0
    else:
        try:
            df = df.rename(columns={'participationFactor':'PARTFACTOR'})
        except:
            pass
    
    df.columns = [x.upper() for x in df.columns]

    busmemos = busdf[['CLEANMEMO','BUSNUM','AREANAME']].values
    busnum_dict = {}
    areaname_dict = {}

    for x in busmemos:
        busnum_dict[x[0]]=x[1]
        areaname_dict[x[0]]=x[2]

    df['BUSNUM'] = df.apply(lambda row: int(busnum_dict[str(row['BUSNAME'])]),axis = 1)
    df['AREA'] = df.apply(lambda row: str(areaname_dict[str(row['BUSNAME'])]),axis = 1)
    
    dict_list = df.to_dict(orient='records')
    return dict_list, df 

def prepareForDump(isoStr, inputDir, outputDir, modelFile, outputAuxFile):

    auxStrTmp = '''
    SCRIPT
    {{
    SetCurrentDirectory("{outputdir}", NO);
    
    DeleteFile("{pw_branch_dump}");
    DeleteFile("{pw_bus_dump}");
    DeleteFile("{pw_gen_dump}");
    DeleteFile("{pw_load_dump}");

    OpenCase("{casefile}",PTI);

    SaveData("{pw_branch_dump}", CSV, BRANCH, [Memo, BusNumFrom, BusNameFrom, BusNumTo, BusNameTo, Circuit, Status, BranchDeviceType, IsXF, MWMax, MWFrom, MWTo, LimitMVAA, LimitMVAB, NomkVFrom, NomkVTo,AreaNameFrom, AreaNameTo], []);
    SaveData("{pw_bus_dump}", CSV, BUS, [Memo, Number, Name, Slack, Status, AreaName, NomkV, LoadMW, GenMW], []);
    SaveData("{pw_gen_dump}", CSV, GEN, [Memo, BusNum, BusName, ID, Status, MW, AGC, MWMin, MWMax, CostModel, PartFact, BusAreaName, AreaName], []);
    SaveData("{pw_load_dump}", CSV, LOAD, [Memo, BusNum, BusName, ID, Status, BusAreaName, AreaName, MW], []);    
    
    ExitProgram;
    }}
    '''

    auxStr = auxStrTmp.format(outputdir=outputDir, casefile=modelFile,
                              pw_branch_dump = gs[isoStr]['pw_branch_dumpfile'],
                              pw_bus_dump = gs[isoStr]['pw_bus_dumpfile'],
                              pw_gen_dump = gs[isoStr]['pw_gen_dumpfile'],
                              pw_load_dump = gs[isoStr]['pw_load_dumpfile'])

    outputAuxFileFullPath = "\\".join([outputDir,outputAuxFile])
    outputAuxFileFullPath = outputAuxFileFullPath.replace('\\','//')
    outputAuxFileFullPath = outputAuxFileFullPath.replace('///','//')
    outputAuxFileFullPath = outputAuxFileFullPath.replace('//','\\')
    
    with open(outputAuxFileFullPath, 'w') as file:
        file.write(auxStr)

    shutil.copy("\\".join([inputDir,modelFile]),outputDir)
        
    return outputAuxFileFullPath 
        
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Dumper.')
    parser.add_argument('--iso', action='store', dest = 'isoStr', type=str, default = 'MISO')
    parser.add_argument('--start', action='store', dest = 'startDate', type=str, default = '2022-02-01')
    parser.add_argument('--end', action='store', dest = 'endDate', type=str, default = '2022-02-28')
    args = parser.parse_args()


    if type(args.startDate) == type(None):
        startdate = gs[args.isoStr]['startdate']
    else:
        startdate = args.startDate
    if type(args.endDate) == type(None):
        enddate = gs[args.isoStr]['enddate']
    else:
        enddate = args.endDate
    
    dbstr = 'snowflakedb_write'
    db_engine = create_snowflake_writer_engine(
        account = cd[dbstr]['account'],
        user=cd[dbstr]['user'],
        password=cd[dbstr]['password'],
        role=cd[dbstr]['role'],
        warehouse=cd[dbstr]['warehouse'],
        database=cd[dbstr]['database'],
        schema=cd[dbstr]['schema'])
    
    #NEED TO THINK OF CONFIG FILE FOR THOSE FILES OR HELPER FUNCTION
    #STARTED TO DEFINE FILE SUFFICES IN THE APP CONFIG FILE
    if args.isoStr == 'MISO':
        modelFile = 'PSSEv30_Feb22_AUCTION_Feb22Auc-Off-peak.RAW'
        monelmtsFile = 'MonitoredLinesandTransformers_Feb22_AUCTION_Feb22Auc.csv'
        ctgsFile = 'Contingencies_Feb22_AUCTION_Feb22Auc.csv'
        outgsFile = 'ScheduledOutages_Feb22_AUCTION_Feb22Auc.csv' 
        srcsnkFile = 'SourcesandSinks_Feb22_AUCTION_Feb22Auc.csv'
        emscgtsFile = 'CTG_Posting_Dec2021.csv'
        flgtsFile = 'Flowgates_Feb22_AUCTION_Feb22Auc.csv'
        nmgrmsFile = 'Nomograms_Feb22_AUCTION_Feb22Auc.csv'

    elif args.isoStr == 'SPPISO':
        print("SPPISO is not fully supported yet")
        sys.exit(0)

    else:
        sys.exit(0)
    
    outputDir = gs[args.isoStr]['output_dir']
    inputDir = gs[args.isoStr]['input_dir']

    if args.isoStr == 'MISO':
        PowerWorldBuses = MISOPowerWorldBuses
        PowerWorldGens = MISOPowerWorldGens
        PowerWorldLoads = MISOPowerWorldLoads
        PowerWorldBranches = MISOPowerWorldBranches
        MonitoredElements = MISOMonitoredElements
        Contingencies = MISOContingencies
        ScheduledOutages = MISOScheduledOutages
        Flowgates = MISOFlowgates
        SourceSink = MISOSourceSink
        InjectionGroups = MISOInjectionGroups
        EMSContingencies = MISOEMSContingencies
        Nomograms = MISONomograms
    elif args.isoStr == 'SPPISO':
        PowerWorldBuses = SPPISOPowerWorldBuses
        PowerWorldGens = SPPISOPowerWorldGens
        PowerWorldLoads = SPPISOPowerWorldLoads
        PowerWorldBranches = SPPISOPowerWorldBranches                
        MonitoredElements = SPPISOMonitoredElements
        Contingencies = SPPISOContingencies
        ScheduledOutages = SPPISOScheduledOutages
        Flowgates = SPPISOFlowgates
        SourceSink = SPPISOSourceSink
        InjectionGroups = SPPISOInjectionGroups
        Nomograms = SPPISONomograms

    #FTR MODEL BUS, BRANCH, GEN, LOAD DATA    
    if True:
        modelDumpAux = prepareForDump(isoStr = args.isoStr, outputDir = outputDir, inputDir = inputDir, modelFile = modelFile, outputAuxFile = "hbz.aux")
        print([gs[args.isoStr]['pwexe'],modelDumpAux])
        subprocess.check_call([gs[args.isoStr]['pwexe'],modelDumpAux])
        df1 = parsePowerWorldDumpFile(outputDir+'/'+gs[args.isoStr]['pw_bus_dumpfile'])
        df2 = parsePowerWorldDumpFile(outputDir+'/'+gs[args.isoStr]['pw_gen_dumpfile'], is_gen_or_load = True)
        df3 = parsePowerWorldDumpFile(outputDir+'/'+gs[args.isoStr]['pw_load_dumpfile'], is_gen_or_load = True)
        df4 = parsePowerWorldDumpFile(outputDir+'/'+gs[args.isoStr]['pw_branch_dumpfile'], is_gen_or_load = True)

        ll1, df11 = prepPWBus(df = df1, filename = modelFile)
        ll4, df44 = prepPWBranch(df = df4, filename = modelFile)
        ll2, df22 = prepPWGen(df = df2, filename = modelFile)
        ll3, df33 = prepPWLoad(df = df3, filename = modelFile)

        if True:                
            insert_obj_list(db_engine = db_engine, dict_list = ll1 , classObj = PowerWorldBuses, mode = 'recreate')
            insert_obj_list(db_engine = db_engine, dict_list = ll2 , classObj = PowerWorldGens, mode = 'recreate')
            insert_obj_list(db_engine = db_engine, dict_list = ll3 , classObj = PowerWorldLoads, mode = 'recreate')
            insert_obj_list(db_engine = db_engine, dict_list = ll4 , classObj = PowerWorldBranches, mode = 'recreate')


    sys.exit(0)
    
    #FTR MODEL MONITORED ELEMENTS        
    if True:    
        ll, df = parse_file(inputDir+'/'+monelmtsFile)
        if True:
            insert_obj_list(db_engine = db_engine, dict_list = ll , classObj = MonitoredElements, mode = 'recreate')

    #FTR MODEL CONTINGENCIES        
    if True:
        ll, df = parse_file(inputDir+'/'+ctgsFile)
        if True:
            insert_obj_list(db_engine = db_engine, dict_list = ll , classObj = Contingencies, mode = 'recreate')

    #FTR MODEL SCHEDULED OUTAGES
    if True:
        ll, df = parse_file(inputDir+'/'+outgsFile, is_outages=True, if_flowgates = False, if_nomograms = False)
        if True:
            insert_obj_list(db_engine = db_engine, dict_list = ll , classObj = ScheduledOutages, mode = 'recreate')

    #FTR MODEL FLOWGATES
    if True:
        ll, df = parse_file(inputDir+'/'+flgtsFile, is_outages=False, if_flowgates=True, if_nomograms = False)
        if True:
            insert_obj_list(db_engine = db_engine, dict_list = ll , classObj = Flowgates, mode = 'recreate')

    #FTR MODEL SOURCE&SINK FILE
    if True:
        ll, df = parse_srcsnk(filename = inputDir+'/'+srcsnkFile, busdf = df11)
        if True:
            insert_obj_list(db_engine = db_engine, dict_list = ll , classObj = SourceSink, mode = 'recreate')

    #BUILD INJECTION GROUPS BASED ON SOURCE&SINK FILE
    if True:
        inj_grps = prepInjectionGroups(df = df, busdf = df11, isoStr = args.isoStr)
        if True:
            insert_obj_list(db_engine = db_engine, dict_list = inj_grps , classObj = InjectionGroups, mode = 'recreate')

    #EMS MODEL CONTINGENCIES
    if True:
        ll, df = parse_cimctgs_file(filename = inputDir+'/'+emscgtsFile)
        if True:
            insert_obj_list(db_engine = db_engine, dict_list = ll, classObj = EMSContingencies, mode = 'recreate')

    #FTR MODEL NOMOGRAMS
    if True:
        ll, df = parse_file(filename = inputDir+'/'+nmgrmsFile, is_outages = False, if_flowgates = False, if_nomograms = True)
        if True:
            insert_obj_list(db_engine = db_engine, dict_list = ll, classObj = Nomograms, mode = 'recreate')
