# -*- coding: utf-8 -*-
"""
Created on Fri Mar 19 09:51:54 2021

@author: Maxim.Naglis
"""

from msilib import schema
import os
import sys
import io
import requests
import arrow
import time
import pandas as pd
import datetime
import snowflake.connector
pd.options.display.max_columns = None
import logging
try: 
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup
from retry import retry
from tqdm import tqdm

sys.path.append('C:\\Users\\MAHSHOOQ.MAJEED\\OneDrive - BioUrja Trading LLC\\Documents\\PWSFX\\NYISO_PWSFX_TEST\\pwsfx\\exceptions')
sys.path.append('C:\\Users\\MAHSHOOQ.MAJEED\\OneDrive - BioUrja Trading LLC\\Documents\\PWSFX\\NYISO_PWSFX_TEST\\pwsfx\\config')
sys.path.append('C:\\Users\\MAHSHOOQ.MAJEED\\OneDrive - BioUrja Trading LLC\\Documents\\PWSFX\\NYISO_PWSFX_TEST\\pwsfx\\utils')
sys.path.append('C:\\Users\\MAHSHOOQ.MAJEED\\OneDrive - BioUrja Trading LLC\\Documents\\PWSFX\\NYISO_PWSFX_TEST\\pwsfx\\db')

from utils import timing
from utils import memoize
from exceptions import *
from config import connData as cd
from config import globalSettings as gs
from db import create_snowflake_reader_engine

def getTransmissionOutagesForDateYES(startDate, endDate, isoStr = 'NYISO'):
    
    startDateStr = startDate.format("YYYY-MM-DD")
    endDateStr = endDate.format("YYYY-MM-DD")
    
    if isoStr == 'NYISO':
        sqlStrTmp = '''
                WITH RAW_DATA AS
                (
                  SELECT ISO AS OUTAGE_ISO,
                  TICKETID,
                  FACILITYID AS FACILITY_UID,
                  (CASE WHEN STARTDATE IS NULL THEN PLANNED_STARTDATE ELSE STARTDATE END)::DATE AS STARTDATE,
                  (CASE WHEN ENDDATE IS NULL THEN PLANNED_ENDDATE ELSE ENDDATE END)::DATE AS ENDDATE,
                  LASTCHANGEDATE,
                  REPORTED_NAME AS MONITORED_ELEMENT
                  FROM "YESDB"."YESDATA"."ALL_OUTAGES"
                  WHERE STATUS_DETAIL NOT IN ('Canceled', 'Cancelled')
                  AND TICKETID IS NOT NULL
                )
                SELECT * 
                FROM RAW_DATA
                WHERE OUTAGE_ISO = '{iso}'
                AND DATE_TRUNC(MONTH, STARTDATE) <= '{startDate}'
                AND DATE_TRUNC(MONTH, ENDDATE) >= '{startDate}'
                QUALIFY ROW_NUMBER() OVER (PARTITION BY TICKETID ORDER BY LASTCHANGEDATE DESC)=1

            '''
        
        sqlStr = sqlStrTmp.format(iso=isoStr, startDate=startDateStr, endDate=endDateStr)
        df = None
        
        dbstr = 'snowflakedb_yes'
        db_engine = create_snowflake_reader_engine(account=cd[dbstr]['account'],user=cd[dbstr]['user'],
                                               password=cd[dbstr]['password'],role=cd[dbstr]['role'],
                                               warehouse=cd[dbstr]['warehouse'],
                                               database=cd[dbstr]['database'],
                                               schema=cd[dbstr]['schema'])
        
        
        con = db_engine.connect()
        
        
        try:
            df = pd.read_sql(sqlStr, con = con, index_col = None)
            df['VOLTAGE'] = None
        except:
            import sys, traceback
            exc_type, exc_value, exc_traceback = sys.exc_info()
            print("-"*60)
            print("*** print_tb:")
            traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
            print("*** print_exception:")
            traceback.print_exception(exc_type, exc_value, exc_traceback,
            limit=2, file=sys.stdout, chain=True)
            print("-"*60)
            raise
        finally:
            con.close()
            #db_engine.dispose()
        
    #MN apply date mask to select only outages for the flow dateyes
    #mask1 = (df['PLANNED_STARTDATE'] <= aDateStr)
    #mask2 = (df['PLANNED_ENDDATE'] >= aDateStr)
    #mask = (mask1 & mask2)
    daily_df = df
    
    return daily_df

if __name__ == '__main__':
    
    logging.basicConfig(format = '%(asctime)s %(levelname)s %(filename)s %(funcName)s %(message)s', level = logging.DEBUG)  

    #caseName = getLatestCaseWithCoordinates(isoStr = "SPP")
    df = getTransmissionOutagesForDateYES(startDate = startDate, endDate = endDate, isoStr = 'NYISO')

