# -*- coding: utf-8 -*-
"""
Created on Tue Nov  31 10:08:38 2021

@author: Maxim.Naglis
"""
import sys
import logging
from sqlalchemy import create_engine
from snowflake.sqlalchemy import URL
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy import *
from sqlalchemy.dialects.mysql import BIT

sys.path.append('C:\\Users\\MAHSHOOQ.MAJEED\\OneDrive - BioUrja Trading LLC\\Documents\\PWSFX\\NYISO_PWSFX_TEST\\pwsfx\\config')
sys.path.append('C:\\Users\\MAHSHOOQ.MAJEED\\OneDrive - BioUrja Trading LLC\\Documents\\PWSFX\\NYISO_PWSFX_TEST\\pwsfx\\db')

from config import connData as cd
from db import create_snowflake_writer_engine

Base = declarative_base()

def insert_obj_list(db_engine, dict_list, classObj, mode = 'recreate'):

    if mode == 'recreate':
        try:
            classObj.__table__.drop(bind = db_engine)
        except:
            pass
        classObj.__table__.create(bind = db_engine)
    if mode == 'update':
        try:
            classObj.__table__.create(bind = db_engine, checkfirst = True)
        except:
            pass

    Session = sessionmaker(bind=db_engine)
    session = Session()

    try:
        count = 0
        for datadict in dict_list:
            count = count + 1
            if datadict != {}:
                record = classObj(**datadict)
                session.add(record)
                if count%1000 == 0:
                    session.commit()
        session.commit()
    except:
        import sys, traceback
        exc_type, exc_value, exc_traceback = sys.exc_info()
        print("-"*60)
        print("*** print_tb:")
        traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
        print("*** print_exception:")
        traceback.print_exception(exc_type, exc_value, exc_traceback,
                                  limit=2, file=sys.stdout, chain=True)
        print("-"*60)
        session.rollback()
        
        try:
            count = 0
            for datadict in dict_list:
                count = count + 1
                if datadict != {}:
                    record = classObj(**datadict)
                    session.merge(record)
                    if count%1000 == 0:
                        session.commit()
            session.commit()
        except:
            import sys, traceback
            exc_type, exc_value, exc_traceback = sys.exc_info()
            print("-"*60)
            print("*** print_tb:")
            traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
            print("*** print_exception:")
            traceback.print_exception(exc_type, exc_value, exc_traceback,
                                      limit=2, file=sys.stdout, chain=True)
            print("-"*60)
            session.rollback()
        
    session.close()

class SFX_Daily(Base):
    __tablename__ = 'PWSFX_SFX_DAILY'
    BU_ISO = Column(String(10), primary_key=True, nullable=False, autoincrement=False)
    #CNX = Column(String(200), primary_key=True, nullable=False, autoincrement=False)
    BU_CID = Column(BigInteger, primary_key=False, nullable=True, autoincrement=False)
    CID = Column(BigInteger, primary_key=False, nullable=True, autoincrement=False)
    #CTG = Column(String(200), primary_key=False, nullable=True, autoincrement=False)
    NODE = Column(String(200), primary_key=True, nullable=False, autoincrement=False)
    SFX = Column(Numeric(12,6), primary_key=False, nullable=True, autoincrement=False)
    #RUN_TYPE = Column(String(10), primary_key=True, nullable=False, autoincrement=False)
    RUNID = Column(Integer, primary_key=True, nullable=False, autoincrement=False)
    #FLOWDATE = Column(Date, primary_key=True, nullable=False, autoincrement=False)
    FLOWMONTH = Column(Date, primary_key=True, nullable=False, autoincrement=False)
    #ENDDATE = Column(Date, primary_key=True, nullable=False, autoincrement=False)        
    
class MISO_UoC(Base):
    __tablename__ = 'PWSFX_MISO_UOC'
    ID = Column(BigInteger, primary_key=True, nullable=False, autoincrement=False)
    CID = Column(BigInteger, primary_key=False, nullable=True, autoincrement=False)
    FULL_INTERFACE_NAME = Column(String(200), primary_key=False, nullable=True, autoincrement=False)
    CONSTRAINT_NAME = Column(String(200), primary_key=True, nullable=False, autoincrement=False)
    CONTINGENCY_UID = Column(String(200), primary_key=True, nullable=False, autoincrement=False)
    MONEL_MEMO = Column(String(200), primary_key=True, nullable=False, autoincrement=False)
    CONVENTION = Column(Integer, primary_key=True, nullable=False, autoincrement=False)

class SPPISO_UoC(Base):
    __tablename__ = 'PWSFX_SPPISO_UOC'
    ID = Column(BigInteger, primary_key=True, nullable=False, autoincrement=False)
    CID = Column(BigInteger, primary_key=False, nullable=True, autoincrement=False)
    FULL_INTERFACE_NAME = Column(String(200), primary_key=False, nullable=True, autoincrement=False)
    CONSTRAINT_NAME = Column(String(200), primary_key=True, nullable=False, autoincrement=False)
    CONTINGENCY_UID = Column(String(200), primary_key=True, nullable=False, autoincrement=False)
    MONEL_MEMO = Column(String(200), primary_key=True, nullable=False, autoincrement=False)
    CONVENTION = Column(Integer, primary_key=True, nullable=False, autoincrement=False)    
    
class UniverseOfConstraints(Base):
    __tablename__ = 'PWSFX_UOC'
    
    BU_ISO = Column(String(10), primary_key=True, nullable=False, autoincrement=False)
    BU_CID = Column(BigInteger, primary_key=True, nullable=False, autoincrement=False)
    CID = Column(BigInteger, primary_key=False, nullable=True, autoincrement=False)
    FULL_INTERFACE_NAME = Column(String(200), primary_key=False, nullable=True, autoincrement=False)
    CONSTRAINT_NAME = Column(String(200), primary_key=True, nullable=False, autoincrement=False)
    CONTINGENCY_UID = Column(String(200), primary_key=False, nullable=True, autoincrement=False)
    MONEL_MEMO = Column(String(200), primary_key=False, nullable=True, autoincrement=False)
    CONVENTION = Column(Integer, primary_key=False, nullable=True, autoincrement=False)
    FLOW_STARTDATE = Column(Date, primary_key=True, nullable=False, autoincrement=False)
    FLOW_ENDDATE = Column(Date, primary_key=True, nullable=False, autoincrement=False)
    INSERT_DATE = Column(DateTime, primary_key=True, nullable=False, autoincrement=False)
    
class PlannedOutages(object):

    #__tablename__ = 'PWSFX_PLANNEDOUTAGES'
    ISO = Column(String(10), primary_key=True, nullable=False, autoincrement=False)
    OUTAGE_ISO = Column(String(10), primary_key=True, nullable=False, autoincrement=False)
    TICKETID = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    FACILITY_UID = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    TARGET_DATE = Column(Date, primary_key=True, nullable=False, autoincrement=False)
    PLANNED_STARTDATE = Column(DateTime, primary_key=False, nullable=True, autoincrement=False)
    PLANNED_ENDDATE = Column(DateTime, primary_key=False, nullable=True, autoincrement=False)
    LASTCHANGEDATE = Column(DateTime, primary_key=False, nullable=True, autoincrement=False)
    VOLTAGE = Column(Numeric(5,1), primary_key=False, nullable=True, autoincrement=False)
    MONITORED_ELEMENT = Column(String(100), primary_key=False, nullable=True, autoincrement=False)

class MISOPlannedOutages(PlannedOutages,Base):
    __tablename__ = 'PWSFX_MISO_PLANNEDOUTAGES'

class SPPISOPlannedOutages(PlannedOutages,Base):
    __tablename__ = 'PWSFX_SPPISO_PLANNEDOUTAGES'    

class NYISOPlannedOutages(PlannedOutages,Base):
    __tablename__ = 'PWSFX_NYISO_PLANNEDOUTAGES'   
    
class InjectionGroups(object):

    #__tablename__ = 'PWSFX_INJGRUOPS'
    ID = Column(Integer, primary_key=True, nullable=False, autoincrement=True)
    OBJECT = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    CONTAINEDBY = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    PARFAC = Column(Numeric(12,6), primary_key=False, nullable=False, autoincrement=False)

class MISOInjectionGroups(InjectionGroups,Base):
    __tablename__ = 'PWSFX_MISO_INJGRUOPS'

class SPPISOInjectionGroups(InjectionGroups,Base):
    __tablename__ = 'PWSFX_SPPISO_INJGRUOPS'


#Nomogram report snippets go below    
#MISO
#Name	Class	Contingency	Constraint	Limit	Device	Type	Direction	Factor
#1531:TEKAMAH-SUB1216 FLO FORT CALHOUN-RAUN	Peak	MECOPP1	1531:TEKAMAH-SUB1216 FLO FORT CALHOUN-RAUN	32.9	TEKAMSUB1216_1 1	Line	From-To	1
#1532:TEKAMAH-SUB1216 FLO FORT CALHOUN-RAUN	Peak	MECOPP1	1532:TEKAMAH-SUB1216 FLO FORT CALHOUN-RAUN	60	TEKAMSUB1216_1 1	Line	To-From	1
#155114:BCKBONE BCKBODUNDE69_1 1 BASE	Peak		155114:BCKBONE BCKBODUNDE69_1 1 BASE	55.4	BCKBODUNDE69_1 1	Line	From-To	1
#157062:14L TAP-HIBBING FLO 78L TAP	Peak	MP11030	157062:14L TAP-HIBBING FLO 78L TAP	84	14L_THIBBI11_1 1	Line	From-To	1
#SPPISO
#Name	Contingency	ContingencySelection	Class	MarketName	ConstraintName	CommercialFlowConstraint	BaseCaseLimit	EmergencyLimit	Device	DeviceType	Direction	Factor
#VMA		Include	ON	2021_Monthly_TCR_Auction_12_PROD	VMA_ANADARKO_XF5	No	140	140	ANADARKO XG5 XG5 XF	Transformer	From-To	1
#VMA		Include	ON	2021_Monthly_TCR_Auction_12_PROD	VMA_ANTELOP_1	No	480	576	ANTELOP TR1 TR1 XF	Transformer	From-To	1
#VMA		Include	ON	2021_Monthly_TCR_Auction_12_PROD	VMA_ANTELOP_2	No	480	576	ANTELOP TR2 TR2 XF	Transformer	From-To	1
#VMA		Include	ON	2021_Monthly_TCR_Auction_12_PROD	VMA_BNCR	No	57	63	BNCR BNCRTBNCR69_1 1 LN	Line	To-From	1

class Nomograms(object):

    #__tablename__ = 'PWSFX_NMGRMS'
    CID = Column(BigInteger, primary_key=False, nullable=True, autoincrement=False)
    CONSTRAINT_NAME = Column(String(100), primary_key=False, nullable=True, autoincrement=False)
    FGNAME = Column(String(100), primary_key=False, nullable=True, autoincrement=False)
    DEVICENAME = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    DEVICETYPE = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    DIRECTION = Column(String(10), primary_key=False, nullable=True, autoincrement=False)
    LIMIT = Column(Numeric(7,1), primary_key=False, nullable=True, autoincrement=False)
    CONTINGENCY = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    CLASS = Column(String(10), primary_key=True, nullable=False, autoincrement=False)
    CONVENTION = Column(Integer, primary_key=True, nullable=False, autoincrement=False)
    FILENAME = Column(String(200), primary_key=True, nullable=False, autoincrement=False)    

class MISONomograms(Nomograms,Base):    
    __tablename__ = 'PWSFX_MISO_NMGRMS'

class SPPISONomograms(Nomograms,Base):    
    __tablename__ = 'PWSFX_SPPISO_NMGRMS'    

    
#Flowgates       
#MISO
#Name	Type	ContingencyName	Flowgate_Interface	StartDate	EndDate	Limit	Class	DeviceName	FlowDirection
#6314	OTDF	EE50X63	Interface	12/1/2021	12/31/2021	103.5	Peak	TRUSSL_VIENNA3 A	From-To
#6546	OTDF	EE13129	Interface	12/1/2021	12/31/2021	100.8	Peak	DOBBIN_TUBULR4 B	To-From
#1071	OTDF	AMO16X02	Interface	12/1/2021	12/31/2021	205.2	Peak	ENON_ETHLYN    1	From-To
#2609	OTDF	TVA50X32	Interface	12/1/2021	12/31/2021	1558.8	Peak	WESTPNT_CLAY   1	From-To
#SPPISO
#Name	Type	ContingencyName	Limit	Class	DeviceName	FlowDirection	MarketName
#TMP320_26859	OTDF	CON85	355	ON	JUNCTN5 BROOK-NICHOL A LN	To-From	2021_Monthly_TCR_Auction_12_PROD
#PATFULSARLON	OTDF	27751	159	ON	PAT_WX FULTON_PATMO A LN	From-To	2021_Monthly_TCR_Auction_12_PROD
#TMP599_25842	OTDF	4305	499	ON	BISMARK2 WARDWBISMA23_1 1 LN	To-From	2021_Monthly_TCR_Auction_12_PROD
#TEMP23_22895	OTDF	17569	270	ON	NSES 10291 A LN	From-To	2021_Monthly_TCR_Auction_12_PROD

class Flowgates(object):

    #__tablename__ = 'PWSFX_FLGTS'
    NAME = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    TYPE = Column(String(10), primary_key=True, nullable=False, autoincrement=False)
    FLOWGATE_INTERFACE = Column(String(100), primary_key=False, nullable=True, autoincrement=False)
    CONTINGENCYNAME = Column(String(10), primary_key=True, nullable=False, autoincrement=False)
    STARTDATE = Column(Date, primary_key=False, nullable=True, autoincrement=False)
    ENDDATE = Column(Date, primary_key=False, nullable=True, autoincrement=False)
    LIMIT = Column(Numeric(10,3), primary_key=False, nullable=True, autoincrement=False)
    CLASS = Column(String(10), primary_key=True, nullable=False, autoincrement=False)
    DEVICENAME = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    FLOWDIRECTION = Column(String(10), primary_key=True, nullable=False, autoincrement=False)
    MARKETNAME = Column(String(100), primary_key=False, nullable=True, autoincrement=False)
    FILENAME = Column(String(200), primary_key=True, nullable=False, autoincrement=False)

class MISOFlowgates(Flowgates,Base):
    __tablename__ = 'PWSFX_MISO_FLGTS'

class SPPISOFlowgates(Flowgates,Base):
    __tablename__ = 'PWSFX_SPPISO_FLGTS'    
    
class SourceSink(object):

    #__tablename__ = 'PWSFX_SRCSNK'
    PNODENAME = Column(String(45), primary_key=True, nullable=False, autoincrement=False)
    TYPE = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    CLASS = Column(String(10), primary_key=True, nullable=False, autoincrement=False)
    PRICENODE = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    BUSNAME = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    PARTFACTOR = Column(Numeric(10,5), primary_key=False, nullable=True, autoincrement=False)
    BUSNUM = Column(Integer, primary_key=True, nullable=False, autoincrement=False)   
    AREA = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    MARKETNAME = Column(String(100), primary_key=False, nullable=True, autoincrement=False)
    FILENAME = Column(String(200), primary_key=True, nullable=False, autoincrement=False)    

class MISOSourceSink(SourceSink,Base):
    __tablename__ = 'PWSFX_MISO_SRCSNK'

class SPPISOSourceSink(SourceSink,Base):
    __tablename__ = 'PWSFX_SPPISO_SRCSNK'
    
class ScheduledOutages(object):

    #__tablename__ = 'PWSFX_MODELOUTAGES'
    TICKETID = Column(String(20), primary_key=True, nullable=False, autoincrement=False) #MISO Outage field, SPPISO Name field
    DEVICENAME = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    DEVICETYPE = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    CLASS = Column(String(10), primary_key=True, nullable=False, autoincrement=False)
    STARTDATE = Column(Date, primary_key=False, nullable=True, autoincrement=False)
    ENDDATE = Column(Date, primary_key=False, nullable=True, autoincrement=False)
    MARKETNAME = Column(String(100), primary_key=False, nullable=True, autoincrement=False)
    FILENAME = Column(String(200), primary_key=True, nullable=False, autoincrement=False)    

class MISOScheduledOutages(ScheduledOutages,Base):
    __tablename__ = 'PWSFX_MISO_MODELOUTAGES'

class SPPISOScheduledOutages(ScheduledOutages,Base):
    __tablename__ = 'PWSFX_SPPISO_MODELOUTAGES'    

#Index(['CTGID', 'CTG_FACILITY', 'EQTYPE', 'KEY1', 'KEY2', 'KEY3', 'ACTION','IDCMAPPING', 'MEMONOSPACE', 'FILENAME'],
class EMSContingencies(object):

    #__tablename__ = 'PWSFX_EMSCTGS'
    CTGID = Column(String(10), primary_key=True, nullable=False, autoincrement=False)
    CTG_FACILITY = Column(String(100), primary_key=False, nullable=True, autoincrement=False)
    EQTYPE = Column(String(10), primary_key=True, nullable=False, autoincrement=False)
    KEY1 = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    KEY2 = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    KEY3 = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    ACTION = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    IDCMAPPING = Column(String(100), primary_key=False, nullable=True, autoincrement=False)
    MEMONOSPACE = Column(String(100), primary_key=False, nullable=True, autoincrement=False)
    FILENAME = Column(String(200), primary_key=True, nullable=False, autoincrement=False)

class MISOEMSContingencies(EMSContingencies,Base):
    __tablename__ = 'PWSFX_MISO_EMSCTGS'
    
class Contingencies(object):

    #__tablename__ = 'PWSFX_CTGS'
    CONTINGENCY = Column(String(10), primary_key=True, nullable=False, autoincrement=False)
    DEVICENAME = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    DEVICETYPE = Column(String(10), primary_key=True, nullable=False, autoincrement=False)
    FROMBUS = Column(String(100), primary_key=False, nullable=True, autoincrement=False)
    TOBUS = Column(String(100), primary_key=False, nullable=True, autoincrement=False)
    CIRCUITID = Column(String(10), primary_key=False, nullable=True, autoincrement=False)
    ACTION = Column(String(10), primary_key=True, nullable=False, autoincrement=False)
    CLASS = Column(String(10), primary_key=True, nullable=False, autoincrement=False)
    MARKETNAME = Column(String(100), primary_key=False, nullable=True, autoincrement=False)
    FILENAME = Column(String(200), primary_key=True, nullable=False, autoincrement=False)

class MISOContingencies(Contingencies,Base):
    __tablename__ = 'PWSFX_MISO_CTGS'

class SPPISOContingencies(Contingencies,Base):
    __tablename__ = 'PWSFX_SPPISO_CTGS'    
    
class MonitoredElements(object):

    #__tablename__ = 'PWSFX_MONELMNTS'
    DEVICENAME = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    DEVICETYPE = Column(String(10), primary_key=True, nullable=False, autoincrement=False)
    CLASS = Column(String(10), primary_key=True, nullable=False, autoincrement=False)
    BASECASERATING = Column(Numeric(10,3), primary_key=False, nullable=True, autoincrement=False)
    EMERGENCYRATING = Column(Numeric(10,3), primary_key=False, nullable=True, autoincrement=False)
    MARKETNAME = Column(String(100), primary_key=False, nullable=True, autoincrement=False)
    FILENAME = Column(String(200), primary_key=True, nullable=False, autoincrement=False)

class MISOMonitoredElements(MonitoredElements,Base):
    __tablename__ = 'PWSFX_MISO_MONELMNTS'

class SPPISOMonitoredElements(MonitoredElements,Base):
    __tablename__ = 'PWSFX_SPPISO_MONELMNTS'    

    
class PowerWorldBuses(object):

    #__tablename__ = 'PWSFX_PWBUSES'
    MEMO = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    CLEANMEMO = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    BUSNAME = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    BUSNUM = Column(Integer, primary_key=True, nullable=False, autoincrement=False)
    SLACK = Column(String(10), primary_key=True, nullable=False, autoincrement=False)
    STATUS = Column(String(20), primary_key=True, nullable=False, autoincrement=False)
    AREANAME = Column(String(20), primary_key=False, nullable=False, autoincrement=False)
    NOMKV = Column(Integer, primary_key=False, nullable=False, autoincrement=False)
    LOADMW = Column(Numeric(10,3), primary_key=False, nullable=True, autoincrement=False)
    GENMW = Column(Numeric(10,3), primary_key=False, nullable=True, autoincrement=False)  
    FILENAME = Column(String(200), primary_key=True, nullable=False, autoincrement=False)       

class MISOPowerWorldBuses(PowerWorldBuses,Base):
    __tablename__ = 'PWSFX_MISO_PWBUSES'

class SPPISOPowerWorldBuses(PowerWorldBuses,Base):
    __tablename__ = 'PWSFX_SPPISO_PWBUSES'        
    
class PowerWorldBranches(object):

    #__tablename__ = 'PWSFX_PWBRANCHES'    
    MEMO = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    CLEANMEMO = Column(String(100), primary_key=True, nullable=False, autoincrement=False)   
    BUSNUMFROM = Column(Integer, primary_key=True, nullable=False, autoincrement=False)
    BUSNUMTO = Column(Integer, primary_key=True, nullable=False, autoincrement=False)
    BUSNAMEFROM = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    BUSNAMETO = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    CIRCUIT = Column(String(10), primary_key=True, nullable=False, autoincrement=False)
    STATUS = Column(String(20), primary_key=True, nullable=False, autoincrement=False)
    BRANCHDEVICETYPE = Column(String(20), primary_key=True, nullable=False, autoincrement=False)
    ISXF = Column(String(10), primary_key=True, nullable=False, autoincrement=False)
    AREANAMEFROM = Column(String(20), primary_key=False, nullable=False, autoincrement=False)
    AREANAMETO = Column(String(20), primary_key=False, nullable=False, autoincrement=False)
    NOMKVFROM = Column(Integer, primary_key=False, nullable=False, autoincrement=False)
    NOMKVTO = Column(Integer, primary_key=False, nullable=False, autoincrement=False)
    MWMAX = Column(Numeric(10,3), primary_key=False, nullable=False, autoincrement=False)
    MWFROM = Column(Numeric(10,3), primary_key=False, nullable=False, autoincrement=False)
    MWTO = Column(Numeric(10,3), primary_key=False, nullable=False, autoincrement=False)
    LIMITMVAA = Column(Integer, primary_key=False, nullable=False, autoincrement=False)
    LIMITMVAB = Column(Integer, primary_key=False, nullable=False, autoincrement=False)
    FILENAME = Column(String(200), primary_key=True, nullable=False, autoincrement=False)     

class MISOPowerWorldBranches(PowerWorldBranches,Base):
    __tablename__ = 'PWSFX_MISO_PWBRANCHES'

class SPPISOPowerWorldBranches(PowerWorldBranches,Base):
    __tablename__ = 'PWSFX_SPPISO_PWBRANCHES'        
    
class PowerWorldGens(object):   

    #__tablename__ = 'PWSFX_PWGENS' 
    MEMO = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    CLEANMEMO = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    BUSNAME = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    BUSNUM = Column(Integer, primary_key=True, nullable=False, autoincrement=False)   
    GENID = Column(String(10), primary_key=True, nullable=False, autoincrement=False)
    STATUS = Column(String(20), primary_key=True, nullable=False, autoincrement=False)
    AGC = Column(String(10), primary_key=True, nullable=False, autoincrement=False)
    MW = Column(Numeric(10,3), primary_key=False, nullable=True, autoincrement=False)    
    MWMIN = Column(Numeric(10,3), primary_key=False, nullable=False, autoincrement=False)
    MWMAX = Column(Numeric(10,3), primary_key=False, nullable=False, autoincrement=False) 
    AREANAME = Column(String(20), primary_key=False, nullable=False, autoincrement=False)
    BUSAREANAME = Column(String(20), primary_key=False, nullable=False, autoincrement=False)
    COSTMODEL = Column(String(20), primary_key=False, nullable=True, autoincrement=False)
    PARTFACT = Column(Numeric(10,3), primary_key=False, nullable=False, autoincrement=False) 
    FILENAME = Column(String(200), primary_key=True, nullable=True, autoincrement=False)     

class MISOPowerWorldGens(PowerWorldGens,Base):   
    __tablename__ = 'PWSFX_MISO_PWGENS'

class SPPISOPowerWorldGens(PowerWorldGens,Base):   
    __tablename__ = 'PWSFX_SPPISO_PWGENS'     
    
class PowerWorldLoads(object):   
    
    #__tablename__ = 'PWSFX_PWLOADS' 
    MEMO = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    CLEANMEMO = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    BUSNAME = Column(String(100), primary_key=True, nullable=False, autoincrement=False)
    BUSNUM = Column(Integer, primary_key=True, nullable=False, autoincrement=False)   
    LOADID = Column(String(10), primary_key=True, nullable=False, autoincrement=False)
    STATUS = Column(String(20), primary_key=True, nullable=False, autoincrement=False)
    MW = Column(Numeric(10,3), primary_key=False, nullable=True, autoincrement=False)
    AREANAME = Column(String(20), primary_key=False, nullable=False, autoincrement=False)
    BUSAREANAME = Column(String(20), primary_key=False, nullable=False, autoincrement=False)   
    FILENAME = Column(String(200), primary_key=True, nullable=False, autoincrement=False)     

class MISOPowerWorldLoads(PowerWorldLoads,Base):       
    __tablename__ = 'PWSFX_MISO_PWLOADS'

class SPPISOPowerWorldLoads(PowerWorldLoads,Base):       
    __tablename__ = 'PWSFX_SPPISO_PWLOADS'     
    
if __name__ == '__main__':

    dbstr = 'snowflakedb_write'
    db_engine = create_snowflake_writer_engine(
        account = cd[dbstr]['account'],
        user=cd[dbstr]['user'],
        password=cd[dbstr]['password'],
        role=cd[dbstr]['role'],
        warehouse=cd[dbstr]['warehouse'],
        database=cd[dbstr]['database'],
        schema=cd[dbstr]['schema'])


    if False:
        try:
            MISOPlannedOutages.__table__.drop(bind=db_engine)
            SPPISOPlannedOutages.__table__.drop(bind=db_engine)
        
            MISOInjectionGroups.__table__.drop(bind=db_engine)
            SPPISOInjectionGroups.__table__.drop(bind=db_engine)
        
            MISONomograms.__table__.drop(bind=db_engine)
            SPPISONomograms.__table__.drop(bind=db_engine)
            
            MISOFlowgates.__table__.drop(bind=db_engine)
            SPPISOFlowgates.__table__.drop(bind=db_engine)
            
            MISOSourceSink.__table__.drop(bind=db_engine)
            SPPISOSourceSink.__table__.drop(bind=db_engine)
            
            MISOScheduledOutages.__table__.drop(bind=db_engine)
            SPPISOScheduledOutages.__table__.drop(bind=db_engine)
            
            MISOEMSContingencies.__table__.drop(bind=db_engine)
            
            MISOContingencies.__table__.drop(bind=db_engine)
            SPPISOContingencies.__table__.drop(bind=db_engine)
            
            MISOMonitoredElements.__table__.drop(bind=db_engine)
            SPPISOMonitoredElements.__table__.drop(bind=db_engine)
            
            MISOPowerWorldBuses.__table__.drop(bind=db_engine)
            SPPISOPowerWorldBuses.__table__.drop(bind=db_engine)
            
            MISOPowerWorldBranches.__table__.drop(bind=db_engine)
            SPPISOPowerWorldBranches.__table__.drop(bind=db_engine)
            
            MISOPowerWorldGens.__table__.drop(bind=db_engine)
            SPPISOPowerWorldGens.__table__.drop(bind=db_engine)
            
            MISOPowerWorldLoads.__table__.drop(bind=db_engine)
            SPPISOPowerWorldLoads.__table__.drop(bind=db_engine)        
        except:
            pass
        finally:
            pass

    if False:
        try:
            MISOPlannedOutages.__table__.create(bind=db_engine, checkfirst=True)
            SPPISOPlannedOutages.__table__.create(bind=db_engine, checkfirst=True)
            
            MISOInjectionGroups.__table__.create(bind=db_engine, checkfirst=True)
            SPPISOInjectionGroups.__table__.create(bind=db_engine, checkfirst=True)
            
            MISONomograms.__table__.create(bind=db_engine, checkfirst=True)
            SPPISONomograms.__table__.create(bind=db_engine, checkfirst=True)

            MISOFlowgates.__table__.create(bind=db_engine, checkfirst=True)
            SPPISOFlowgates.__table__.create(bind=db_engine, checkfirst=True)

            MISOSourceSink.__table__.create(bind=db_engine, checkfirst=True)
            SPPISOSourceSink.__table__.create(bind=db_engine, checkfirst=True)

            MISOScheduledOutages.__table__.create(bind=db_engine, checkfirst=True)
            SPPISOScheduledOutages.__table__.create(bind=db_engine, checkfirst=True)

            MISOEMSContingencies.__table__.create(bind=db_engine, checkfirst=True)

            MISOContingencies.__table__.create(bind=db_engine, checkfirst=True)
            SPPISOContingencies.__table__.create(bind=db_engine, checkfirst=True)

            MISOMonitoredElements.__table__.create(bind=db_engine, checkfirst=True)
            SPPISOMonitoredElements.__table__.create(bind=db_engine, checkfirst=True)

            MISOPowerWorldBuses.__table__.create(bind=db_engine, checkfirst=True)
            SPPISOPowerWorldBuses.__table__.create(bind=db_engine, checkfirst=True)

            MISOPowerWorldBranches.__table__.create(bind=db_engine, checkfirst=True)
            SPPISOPowerWorldBranches.__table__.create(bind=db_engine, checkfirst=True)

            MISOPowerWorldGens.__table__.create(bind=db_engine, checkfirst=True)
            SPPISOPowerWorldGens.__table__.create(bind=db_engine, checkfirst=True)

            MISOPowerWorldLoads.__table__.create(bind=db_engine, checkfirst=True)
            SPPISOPowerWorldLoads.__table__.create(bind=db_engine, checkfirst=True)

            UniverseOfConstraints.__table__.create(bind = db_engine, checkfirst = True)
            MISO_UoC.__table__.create(bind = db_engine, checkfirst = True)
            SPPISO_UoC.__table__.create(bind = db_engine, checkfirst = True)
        except:
            raise

