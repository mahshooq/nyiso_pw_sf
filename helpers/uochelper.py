import sys
import os
import numpy as np
import subprocess
import argparse
import shutil
import glob
import decimal
from time import strftime,localtime
import pandas as pd
pd.options.display.max_columns = None

from pwsfx.exceptions.exceptions import *
from pwsfx.config.config import connData as cd
from pwsfx.db.db import create_snowflake_writer_engine
from pwsfx.db.db import create_snowflake_reader_engine
#from pwsfx.db.db import get_conn
from pwsfx.db.db import execute_query

from pwsfx.config.config import globalSettings as gs
from pwsfx.helpers.sfhelper import insert_obj_list, UniverseOfConstraints
from pwsfx.helpers.sfhelper import MISO_UoC, SPPISO_UoC

def insertUOCforPeriodToSF(uoc, dbstr = 'snowflakedb_write'):

    uoc['INSERT_DATE'] =   strftime("%Y-%m-%d %H:%M:%S", localtime())
    uoc_dictlist = uoc.to_dict(orient='records')

    db_engine = create_snowflake_writer_engine(
        account = cd[dbstr]['account'],
        user=cd[dbstr]['user'],
        password=cd[dbstr]['password'],
        role=cd[dbstr]['role'],
        warehouse=cd[dbstr]['warehouse'],
        database=cd[dbstr]['database'],
        schema=cd[dbstr]['schema'])

    insert_obj_list(db_engine = db_engine, dict_list = uoc_dictlist , classObj = UniverseOfConstraints, mode = 'update')
    return uoc

def generateUOCforPeriod(dbstr = 'snowflakedb', iso = "MISO", startdate = '2022-01-01',enddate='2022-01-31'):

    sqlStrTmp = '''
    SELECT BU_ISO, BU_CID, CID, --PRETTY_INTERFACE_NAME, 
    FULL_INTERFACE_NAME, CONSTRAINT_NAME, CONTINGENCY_UID, MONEL_MEMO, CONVENTION FROM 
    (
    SELECT 'MISO' AS BU_ISO, 3000000+ID AS BU_CID, CID, --PRETTY_INTERFACE_NAME, 
    FULL_INTERFACE_NAME, CONSTRAINT_NAME, CONTINGENCY_UID, MONEL_MEMO, CONVENTION
    FROM POWERDB_BIZDEV.PTEST.PWSFX_MISO_UOC
    UNION
    SELECT 'SPPISO'AS BU_ISO,2000000+ID AS BU_CID, CID, --PRETTY_INTERFACE_NAME, 
    FULL_INTERFACE_NAME, CONSTRAINT_NAME, CONTINGENCY_UID, MONEL_MEMO, CONVENTION
    FROM POWERDB_BIZDEV.PTEST.PWSFX_SPPISO_UOC
    )
    WHERE BU_ISO = '{isoStr}'
    '''

    sqlStr = sqlStrTmp.format(isoStr=iso)
    
    df = None
    db_engine = create_snowflake_reader_engine(account=cd[dbstr]['account'],user=cd[dbstr]['user'],
                                               password=cd[dbstr]['password'],role=cd[dbstr]['role'],
                                               warehouse=cd[dbstr]['warehouse'],database=cd[dbstr]['database'])
    con = db_engine.connect()
        
    try:
        df = pd.read_sql(sqlStr, con = con, index_col = None)
    except:
        import sys, traceback
        exc_type, exc_value, exc_traceback = sys.exc_info()
        print("-"*60)
        print("*** print_tb:")
        traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
        print("*** print_exception:")
        traceback.print_exception(exc_type, exc_value, exc_traceback,
                                  limit=2, file=sys.stdout, chain=True)
        print("-"*60)
        raise
    finally:
        con.close()
        db_engine.dispose()

    df.columns = [x.upper() for x in df.columns]
    try:
        df['BU_CID'] = df['BU_CID'].astype('int64')
        df['CID'] = df['CID'].astype('int64')
        df['CONSTRAINT_NAME'] = df['CONSTRAINT_NAME'].astype('str')
        df['CONTINGENCY_UID'] = df['CONTINGENCY_UID'].astype('str')
        df['MONEL_MEMO'] = df['MONEL_MEMO'].astype('str')
        df['CONVENTION'] = df['CONVENTION'].astype('int64')
        df['FLOW_STARTDATE'] = startdate
        df['FLOW_ENDDATE'] = enddate
    except:
        raise
        
    return df

def getOldISOUOC(dbstr = 'snowflakedb', iso = "MISO"):

    if iso == 'MISO':
        sqlStrTmp = '''       
SELECT ID, CID, FULL_INTERFACE_NAME, CONSTRAINT_NAME, CONTINGENCY_UID, MONEL_MEMO, CONVENTION
FROM POWERDB_BIZDEV.PTEST.PWSFX_MISO_UOC;
        '''

    sqlStr = sqlStrTmp.format(0)
    
    df = None
    db_engine = create_snowflake_reader_engine(account=cd[dbstr]['account'],user=cd[dbstr]['user'],
                                               password=cd[dbstr]['password'],role=cd[dbstr]['role'],
                                               warehouse=cd[dbstr]['warehouse'],database=cd[dbstr]['database'])
    con = db_engine.connect()
        
    try:
        df = pd.read_sql(sqlStr, con = con, index_col = None)
    except:
        import sys, traceback
        exc_type, exc_value, exc_traceback = sys.exc_info()
        print("-"*60)
        print("*** print_tb:")
        traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
        print("*** print_exception:")
        traceback.print_exception(exc_type, exc_value, exc_traceback,
                                  limit=2, file=sys.stdout, chain=True)
        print("-"*60)
        raise
    finally:
        con.close()
        db_engine.dispose()

    df.columns = [x.upper() for x in df.columns]
    try:
        df['ID'] = df['ID'].astype('int64')
        df['CID'] = df['CID'].astype('int64')
        df['CONSTRAINT_NAME'] = df['CONSTRAINT_NAME'].astype('str')
        df['CONTINGENCY_UID'] = df['CONTINGENCY_UID'].astype('str')
        df['MONEL_MEMO'] = df['MONEL_MEMO'].astype('str')
        df['CONVENTION'] = df['CONVENTION'].astype('int64')
    except:
        pass
        #raise
        
    return df


def getNewISOUOC(dbstr = 'snowflakedb', iso = "MISO"):

    if iso == 'MISO':
        sqlStrTmp = '''       
SELECT CID, FULL_INTERFACE_NAME, CONSTRAINT_NAME, CONTINGENCY_UID, MONEL_MEMO, CONVENTION FROM
(
SELECT * FROM POWERDB_BIZDEV.PTEST.PWSFX_MISO_UOC_HISTORIC_VW
UNION
SELECT * FROM POWERDB_BIZDEV.PTEST.PWSFX_MISO_UOC_NOMOGRAMS_VW
UNION
SELECT * FROM POWERDB_BIZDEV.PTEST.PWSFX_MISO_UOC_BCSFFLOWGATES_VW WHERE MONEL_MEMO IS NOT NULL
UNION
SELECT * FROM POWERDB_BIZDEV.PTEST.PWSFX_MISO_UOC_FLOWGATES_VW
UNION
SELECT * FROM POWERDB_BIZDEV.PTEST.PWSFX_MISO_UOC_NMGRMFLOWGATES_VW
) uoc
        '''

    sqlStr = sqlStrTmp.format(0)
        
    df = None
    db_engine = create_snowflake_reader_engine(account=cd[dbstr]['account'],user=cd[dbstr]['user'],
                                               password=cd[dbstr]['password'],role=cd[dbstr]['role'],
                                               warehouse=cd[dbstr]['warehouse'],database=cd[dbstr]['database'])
    con = db_engine.connect()
    
    try:
        df = pd.read_sql(sqlStr, con = con, index_col = None)
    except:
        import sys, traceback
        exc_type, exc_value, exc_traceback = sys.exc_info()
        print("-"*60)
        print("*** print_tb:")
        traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
        print("*** print_exception:")
        traceback.print_exception(exc_type, exc_value, exc_traceback,
                                  limit=2, file=sys.stdout, chain=True)
        print("-"*60)
        raise
    finally:
        con.close()
        db_engine.dispose()

    df.columns = [x.upper() for x in df.columns]
    try:
        df['CID'] = df['CID'].astype('int64')
        df['CONSTRAINT_NAME'] = df['CONSTRAINT_NAME'].astype('str')
        df['CONTINGENCY_UID'] = df['CONTINGENCY_UID'].astype('str')
        df['MONEL_MEMO'] = df['MONEL_MEMO'].astype('str')
        df['CONVENTION'] = df['CONVENTION'].astype('int64')
    except:
        raise
            
    return df
            
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Dumper.')
    parser.add_argument('--iso', action='store', dest = 'isoStr', type=str, default = 'MISO')
    parser.add_argument('--start', action='store', dest = 'startDate', type=str, default = '2022-02-01')
    parser.add_argument('--end', action='store', dest = 'endDate', type=str, default = '2022-02-28')
    args = parser.parse_args()


    if type(args.startDate) == type(None):
        startdate = gs[args.isoStr]['startdate']
    else:
        startdate = args.startDate
    if type(args.endDate) == type(None):
        enddate = gs[args.isoStr]['enddate']
    else:
        enddate = args.endDate
    
    dbstr = 'snowflakedb_write'
    db_engine = create_snowflake_writer_engine(
        account = cd[dbstr]['account'],
        user=cd[dbstr]['user'],
        password=cd[dbstr]['password'],
        role=cd[dbstr]['role'],
        warehouse=cd[dbstr]['warehouse'],
        database=cd[dbstr]['database'],
        schema=cd[dbstr]['schema'])
    
    
    outputDir = gs[args.isoStr]['output_dir']
    inputDir = gs[args.isoStr]['input_dir']

    new_df = getNewISOUOC()
    old_df = getOldISOUOC()

    if old_df.empty:
        maxid = 1
    else:
        maxid = old_df['ID'].max()+1

        
    new_uoc = new_df.to_dict(orient='records')
    old_uoc = old_df.drop(['ID'],axis=1).to_dict(orient='records')    
    
    delta_uoc = [x for x in new_uoc if x not in old_uoc]
    delta_df = pd.DataFrame(data=delta_uoc, columns=['CID', 'FULL_INTERFACE_NAME', 'CONSTRAINT_NAME','CONTINGENCY_UID', 'MONEL_MEMO', 'CONVENTION'])
    delta_df['CID'] = delta_df['CID'].astype('int64')
    delta_df['CONTINGENCY_UID'] = delta_df['CONTINGENCY_UID'].astype('str')
    delta_df['CONSTRAINT_NAME'] = delta_df['CONSTRAINT_NAME'].astype('str')
    delta_df['MONEL_MEMO'] = delta_df['MONEL_MEMO'].astype('str')
    delta_df['CONVENTION'] = delta_df['CONVENTION'].astype('int64')
    delta_df['ID'] = delta_df.groupby(['FULL_INTERFACE_NAME']).ngroup()
    delta_df['ID'] = delta_df['ID'] + maxid

    ll = delta_df.to_dict(orient="records")
    insert_obj_list(db_engine = db_engine, dict_list = ll , classObj = MISO_UoC, mode = 'update')
    
    df = generateUOCforPeriod(dbstr = 'snowflakedb', iso = "MISO", startdate = startdate, enddate = enddate)
    df = insertUOCforPeriodToSF(uoc = df, dbstr = 'snowflakedb_write')
