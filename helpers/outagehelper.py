import sys
import os
import numpy as np
import subprocess
import argparse
import shutil
import glob
import decimal
import pandas as pd
import arrow
pd.options.display.max_columns = None

sys.path.append('C:\\Users\\MAHSHOOQ.MAJEED\\OneDrive - BioUrja Trading LLC\\Documents\\PWSFX\\NYISO_PWSFX_TEST\\pwsfx\\exceptions')
sys.path.append('C:\\Users\\MAHSHOOQ.MAJEED\\OneDrive - BioUrja Trading LLC\\Documents\\PWSFX\\NYISO_PWSFX_TEST\\pwsfx\\config')
sys.path.append('C:\\Users\\MAHSHOOQ.MAJEED\\OneDrive - BioUrja Trading LLC\\Documents\\PWSFX\\NYISO_PWSFX_TEST\\pwsfx\\db')
sys.path.append('C:\\Users\\MAHSHOOQ.MAJEED\\OneDrive - BioUrja Trading LLC\\Documents\\PWSFX\\NYISO_PWSFX_TEST\\pwsfx\\helpers')
sys.path.append('C:\\Users\\MAHSHOOQ.MAJEED\\OneDrive - BioUrja Trading LLC\\Documents\\PWSFX\\NYISO_PWSFX_TEST\\pwsfx\\yeshelper')

from exceptions import *
from config import connData as cd
from config import globalSettings as gs
from db import create_snowflake_writer_engine
from db import create_snowflake_reader_engine

from sfhelper import insert_obj_list
from sfhelper import MISOPlannedOutages, SPPISOPlannedOutages
#from ..helpers.muhelper import getLatestCaseWithCoordinates
from yeshelper import getTransmissionOutagesForDateYES
    
def generateDateList(startdate, enddate = None, step=1):

    def cleanDate(aDate):    
        return aDate.replace(second=0).replace(microsecond=0)

    start = arrow.get(startdate)
    if enddate == None:
        end = start.replace(day=1).shift(months=1).shift(days=-1)
    else:
        end = arrow.get(enddate)
    
    targetdates = []
    curdate = start
    targetdates.append(curdate)
    while(curdate.shift(days=step) <= end):
        curdate = curdate.shift(days=step)
        targetdates.append(curdate)
    
    return targetdates
    
def fetchOutagesForDate(startDate, endDate, iso = 'NYISO'):

    print("Getting ",iso," outages between ",startDate.format("YYYY-MM-DD"), "and ", endDate.format("YYYY-MM-DD"))
    if iso == 'NYISO':
        isoStr = 'NYISO'

    df = getTransmissionOutagesForDateYES(startDate = startDate, endDate = endDate, isoStr = isoStr)
    df = df.astype(object).where(pd.notnull(df),None)
    df['ISO'] = iso
    df['TARGET_DATE'] = startDate.format("YYYY-MM-DD")
    
    record_list = df.to_dict(orient='records')
    return record_list, df

def mapOutages(outage_df, season='SPRING', year=2022):

    print("Mapping outages to respective from bus and to buses")
    
    sqlStr='''
            SELECT EQ_NAME, 
            FROM_BUS, 
            TO_BUS,
            CIRCUIT_ID
            FROM "POWERDB"."ISO"."NYISO_TCC_FACILITIES"
            WHERE SEASON='{season}' 
            AND YEAR='{year}'
        '''
    sqlStr = sqlStr.format(season=season, year=year)
    facilities = None
    
    dbstr = 'snowflakedb'
    db_engine = create_snowflake_reader_engine(account=cd[dbstr]['account'],user=cd[dbstr]['user'],
                                           password=cd[dbstr]['password'],role=cd[dbstr]['role'],
                                           warehouse=cd[dbstr]['warehouse'],
                                           database=cd[dbstr]['database'],
                                           schema=cd[dbstr]['schema'])
    
    
    con = db_engine.connect()
    
    try:
        facilities = pd.read_sql(sqlStr, con = con, index_col = None)        
    except:
        import sys, traceback
        exc_type, exc_value, exc_traceback = sys.exc_info()
        print("-"*60)
        print("*** print_tb:")
        traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
        print("*** print_exception:")
        traceback.print_exception(exc_type, exc_value, exc_traceback,
        limit=2, file=sys.stdout, chain=True)
        print("-"*60)
        raise
    finally:
        con.close()
    
    facilities['eq_name']=facilities['eq_name'].str.strip()
    outage_df['monitored_element']=outage_df['monitored_element'].str.strip()
    
    mappedOutages=outage_df.merge(facilities, left_on=['monitored_element'], right_on =['eq_name'])
    
    mappedOutages=mappedOutages[['outage_iso',
                                 'ticketid',
                                 'facility_uid',
                                 'startdate',
                                 'enddate',
                                 'lastchangedate',
                                 'monitored_element',
                                 'from_bus',
                                 'to_bus']]
    
    mappedOutages=mappedOutages.rename(columns={'outage_iso':'OUTAGE_ISO', 
                        'ticketid':'TICKETID', 
                        'facility_uid':'FACILITY_UID',
                        'startdate':'STARTDATE',
                        'enddate':'ENDDATE',
                        'lastchangedate':'LASTCHANGEDATE',
                        'monitored_element':'MONITORED_ELEMENT',
                        'from_bus':'FROM_BUS',
                        'to_bus':'TO_BUS'})
    
    mappedOutages=mappedOutages.dropna()
    
    return mappedOutages

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Dumper.')
    parser.add_argument('--iso', action='store', dest = 'isoStr', type=str, default = 'NYISO')
    parser.add_argument('--startdate', action='store', dest = 'startDate', type=str, default = '2022-03-01')
    parser.add_argument('--enddate', action='store', dest = 'endDate', type=str, default = '2022-03-31')
    
    season='SPRING'
    year=2022

    args = parser.parse_args()

    if type(args.startDate) == type(None):
        startdate = gs[args.isoStr]['startdate']
    else:
        startdate = args.startDate
    if type(args.endDate) == type(None):
        enddate = gs[args.isoStr]['enddate']
    else:
        enddate = args.endDate
    
    #Getting the outages from YES
    outage_list, outage_df = fetchOutagesForDate(startDate = args.startDate, endDate = args.endDate, iso = args.isoStr)
    
    print("Outage_df is generated for ",args.isoStr," for flow date between ", args.startDate," and ", args.endDate)
    
    #Mapping the outages with respective from bus abd to buses from NYISO_TCC_FACILITIES
    mappedOutages=mapOutages(outage_df, season='SPRING', year=2022)
