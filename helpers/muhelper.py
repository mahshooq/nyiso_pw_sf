# -*- coding: utf-8 -*-
"""
Created on Fri Mar 19 09:51:54 2021

@author: Maxim.Naglis
"""

import os
import io
import requests
import arrow
import time
import pandas as pd
import datetime
pd.options.display.max_columns = None
import logging
try: 
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup
from retry import retry
from tqdm import tqdm

from pwsfx.utils.utils import timing
from pwsfx.utils.utils import memoize
from pwsfx.exceptions.exceptions import *
from pwsfx.config.config import connData as cd
from pwsfx.config.config import globalSettings as gs

last_call_time = 1
mu_post_sleep_time = 0
mu_get_sleep_time = 0

RMS_AUTH = (cd['muapi']['rms_username'], cd['muapi']['rms_password'])
REFLOW_AUTH = (cd['muapi']['reflow_username'], cd['muapi']['reflow_password'])
REFLOW_AUTH_case = (cd['muapi']['reflow_case_username'], cd['muapi']['reflow_case_password'])

def downloadCase(isoStr = 'MISO', output_dir = gs['global']['output_dir'], case_name ="miso_se_20210309-1800_AREVA"):

    muCaseFormat = "miso-se.xt"
    if isoStr == "MISO": muCaseFormat = "miso-se.xt"
    if isoStr == "SPP": muCaseFormat = "spp-se"

    n_chunk = 10
    urlTmp = 'https://api1.marginalunit.com/reflow/{collection}/{case_name}/download_raw'
    
    url = urlTmp.format(isoStr = isoStr.lower(), collection = muCaseFormat, case_name = case_name ) 
    r = requests.get(url,auth=REFLOW_AUTH_case,stream=False)
    file_size = int(r.headers.get('content-length', 0))
    block_size = 10000 

    filename = os.path.join(output_dir,case_name+'.RAW')
    filename = filename.replace('\\','//')

    with open(filename, 'wb') as file, tqdm(
            desc=filename,
            total=file_size,
            unit='iB',
            unit_scale=True,
            unit_divisor=block_size,
    ) as bar:
        for data in r.iter_content(chunk_size=block_size):
            size = file.write(data)
            bar.update(size)
            time.sleep(0.05)

@memoize(maxsize=10000) 
def getCaseConstraintFlowOverDateRange2(isoStr = "MISO", caseName = "miso_se_20210309-1800_AREVA", listOfOutagedBranchesStr = 'AUBILGAINE13_1 1,RIVERTWILI13_1 1,TOMPKMAJES34_1 1', 
                                       listOfBranchesToEnergizeStr = 'ARTHRMEYER13_1 1,BARNSINVRG69_1 1,BDRA_BULMO11_1 1,BULMOPINER11_1 1,CINC_TWRL_1738 A,ELLENLEOLA11_1 1,FLOWER_BOVINA3 1,FLOWER_OPENWD3 1,FLOWER_TNSLYX3 1,FRKLNT_PAR3    A,HANKSNEWFN23_1 1,MEYERTALLM13_2 1,NEWFNBROWN23_1 1,OVERLBUNGE16_1 1,PAR_HOLTON3    A,S96BARNS69_1   1,STERLSTERL69_1 1,SVLE2TOLRD12_1 1,WLAKEWLAKE69_1 1,X-122          1,X-123          1,X-132          1,X-133          1', 
                                       #listOfGenOutagesStr = 'MCV      MCV_MCV,MCV      OFFSET', monitoredBranch = 'CONVIVERON13_1 1', 
                                       listOfGenOutagesStr = 'MCV      MCV_MCV,MCV      OFFSET', monitoredBranch = 'CONVIVERON13_1 1', 
                                       constraintDirectionCoeff = 1, startDate= arrow.now().shift(years=-1), endDate= arrow.now(), attempt = 0):

#At this moment only complete 100% gen outage is supported. 
#It is also not possible to bring gen that is offline in the referece case back into service

    muCaseFormat = "miso-se.xt"
    if isoStr == "MISO": muCaseFormat = "miso-se.xt"         
    
    startDateStr = startDate.format("YYYY-MM-DD")
    endDateStr = endDate.format("YYYY-MM-DD")   
    
    energizedBranches = listOfBranchesToEnergizeStr
    outagedBranches = listOfOutagedBranchesStr
   
    genOutages = listOfGenOutagesStr
    genOutageMWs = ','.join(['0' for x in range(genOutages.count(',')+1)])
    
    urlTmp = "https://api1.marginalunit.com/reflow/{muCaseFormat}/constraint_flow"
    url = urlTmp.format(muCaseFormat = muCaseFormat, caseName = caseName)

 #JSON object parameters that are accepted by the CONST FLOW API call
 #If there are NO gen outages for a constraint, then we need to handle the API call as it fails 
 #(this does not fail if you dont have any TX outages but has GEN outages)
    if genOutages == '':  #If there are no gen outages for this constraint, then remove the generator outages details from the API parameters
        params_api = {"outaged": outagedBranches, "energized": energizedBranches, "ref_case": caseName, 
                      "monitored": monitoredBranch,"coefficients": constraintDirectionCoeff,
                      "from_case": "miso_se_{startDateStr}-1800_AREVA".format(startDateStr = startDateStr),
                      "to_case": "miso_se_{endDateStr}-1800_AREVA".format(endDateStr = endDateStr),
                      "flow": "solved"}
    else:
        params_api = {"outaged": outagedBranches, "energized": energizedBranches, "ref_case": caseName, 
                      "monitored": monitoredBranch,"coefficients": constraintDirectionCoeff,
                      "from_case": "miso_se_{startDateStr}-1800_AREVA".format(startDateStr = startDateStr),
                      "to_case": "miso_se_{endDateStr}-1800_AREVA".format(endDateStr = endDateStr),
                      "set_gen_injections": genOutages,"gen_pg": genOutageMWs,"flow":"solved"}
                  
    #Get Flows with outages
    df = _fetch_dfm_post(url, params_api)

    try:
        #print(df)
        if isoStr == "MISO":
            df['MARKETDATE'] = df.apply(lambda row: arrow.get(str(row['case'].replace('miso_se_','').replace('_AREVA','')).split('-')[0]).format('YYYY-MM-DD'), axis = 1)
            df['MARKETDATE'] = pd.to_datetime(df['MARKETDATE'])
            df['HOUR'] = df.apply(lambda row: int(int(str(row['case'].replace('miso_se_','').replace('_AREVA','')).split('-')[1])/100), axis = 1)
            #MN we are removing true zoroes because MU gives us 0 for cases line is out
            df['HE'] = df['HOUR'] + 1
            df = df[df['flow'] != 0]
            #df = abs(df["flow"])
            logging.debug(df.head())
    except:
        return None
        
    return df

def getCaseConstraintFlowOverDateRange(isoStr = "MISO", caseName = "miso_se_20210309-1800_AREVA", listOfOutagedBranches = [], 
                                       listOfBranchesToEnergize = [], listOfGenOutages = [], monitoredBranch = None, 
                                       constaraintDirectionCoeff = None, startDate= arrow.now().shift(years=-1), endDate= arrow.now()):
#at this moment only complete 100% gen outage is supported. It is also not possible to bring gen that is offline in the referece case back into service
    muCaseFormat = "miso-se.xt"
    if isoStr == "MISO": muCaseFormat = "miso-se.xt"         
    
    startDateStr = startDate.format("YYYY-MM-DD")
    endDateStr = endDate.format("YYYY-MM-DD")   
    
    energizedBranches = ','.join(list(set(listOfBranchesToEnergize)))
    outagedBranches = ','.join(list(set(listOfOutagedBranches)))
    if isoStr == "MISO":        
        if "MCV      MCV_MCV" in listOfGenOutages:
            listOfGenOutages.append('MCV      OFFSET')
    listOfGenOutages = list(set(listOfGenOutages))       
    genOutages = ','.join(listOfGenOutages)
    genOutageMWs = ','.join(['0' for x in len(listOfGenOutages)])
    
    urlTmp = "https://api1.marginalunit.com/reflow/{muCaseFormat}/constraint_flow"
    url = urlTmp.format(muCaseFormat = muCaseFormat, caseName = caseName)

 #JSON object parameters that are accepted by the CONST FLOW API call
 #If there are NO gen outages for a constraint, then we need to handle the API call as it fails 
 #(this does not fail if you dont have any TX outages but has GEN outages)
    if len(listOfGenOutages)==0: #If there are no gen outages for this constraint, then remove the generator outages details from the API parameters
        params_api = {"outaged": outagedBranches, "energized": energizedBranches, "ref_case": caseName, 
                      "monitored": monitoredBranch,"coefficients": constaraintDirectionCoeff,
                      "from_case": "miso_se_{startDateStr}-1800_AREVA".format(startDateStr = startDateStr),
                      "to_case": "miso_se_{endDateStr}-1800_AREVA".format(endDateStr = endDateStr),
                      "flow": "solved"}
    else:
        params_api = {"outaged": outagedBranches, "energized": energizedBranches, "ref_case": caseName, 
                      "monitored": monitoredBranch,"coefficients": constaraintDirectionCoeff,
                      "from_case": "miso_se_{startDateStr}-1800_AREVA".format(startDateStr = startDateStr),
                      "to_case": "miso_se_{endDateStr}-1800_AREVA".format(endDateStr = endDateStr),
                      "set_gen_injections": genOutages,"gen_pg": genOutageMWs,"flow":"solved"}
                       
    #Get Flows with outages
    df = _fetch_dfm_post(url, params_api)
    
#MN not sure if that log part is ging to work - not sure if _fetch_dfm_post function can return None    
    if df is None:
        with open(r".\log\failed.log", 'a') as fh:
            fh.write("OUTAGED:" + str(params_api['outaged']) +" " + "MONIT_ELEM:" + str(params_api['monitored']))
    else:
        #Get absolute flows to get PERCENTILE value
        #MN remove true zeroes because MU gives us 0th in case line is out
        df = df[df['flow'] != 0]
        df['abs_flow']=abs(df['flow'])
        
    logging.debug(df.head())   
    return df

def getCaseContingencies(isoStr = "MISO", caseName = "miso_se_20210309-1800_AREVA"):
    
    muCaseFormat = "miso-se.xt"
    if isoStr == "MISO": muCaseFormat = "miso-se.xt"      
    
    urlTmp = "https://api1.marginalunit.com/reflow/{muCaseFormat}/{caseName}/contingencies"
    url = urlTmp.format(muCaseFormat = muCaseFormat, caseName = caseName)
    
    resp = requests.get(url,auth=REFLOW_AUTH)
    df = pd.read_csv(io.StringIO(resp.text))
    
    #Load the contingencies in a dataframe called df_ctgs
    df_ctgs = df.groupby('contingency_name')['branch_name'].apply(','.join).reset_index().rename(columns = {'branch_name' : 'Contingency Branches'})
    
    logging.debug(df_ctgs.head())   
    return df_ctgs   

#@memoize(maxsize=10000) #if we want to use memoize - need to convert listOfBranchMemos to string first
def getBranchClosureImpactMultiple(isoStr = "MISO", caseName = "miso_se_20210309-1800_AREVA", listOfBranchMemos = []):
#this function calculates    
    muCaseFormat = "miso-se.xt"
    if isoStr == "MISO": muCaseFormat = "miso-se.xt"  
 
    impactBranches = ','.join(list(set(listOfBranchMemos)))
    
    urlTmp = "https://api1.marginalunit.com/reflow/{muCaseFormat}/{caseName}/branch_closure_impact?outaged={impactBranches}"
    url = urlTmp.format(muCaseFormat = muCaseFormat, caseName = caseName , impactBranches = impactBranches)   
    
    resp = requests.get(url,auth=REFLOW_AUTH)
    df = pd.read_csv(io.StringIO(resp.text))
        
    logging.debug(df.head())
    df = df[~df['pre_flow'].isna()].reset_index(drop=True)
    df['DELTA']= df['post_flow'] - df['pre_flow']
    df = df[abs(df['DELTA'])>0]
    df['NEW_LINE'] = ''
    return df    
 
@memoize(maxsize=10000)  
def getGenOutageExposure2(isoStr = "MISO", caseName = "miso_se_20210309-1800_AREVA", outageBusId = None, listOfTransientBranchesStr = ''):
#outageBusId = busid of the bus nodal exposure calculated for 
    muCaseFormat = "miso-se.xt"
    if isoStr == "MISO": muCaseFormat = "miso-se.xt"  
    
    transientBranches = listOfTransientBranchesStr
    
    urlTmp = "https://api1.marginalunit.com/reflow/{muCaseFormat}/{caseName}/node_exposure/bus"
    url = urlTmp.format(muCaseFormat = muCaseFormat, caseName = caseName)
    
    params = {"source": outageBusId, "energized": transientBranches}
    logging.debug("Downloading GSF for bus {0}".format(outageBusId))
    df = _fetch_dfm_post(url, params)

    df = df.rename(columns={"exposure":outageBusId})
    logging.debug(df.head())   
    return df   

def getGenOutageExposure(isoStr = "MISO", caseName = "miso_se_20210309-1800_AREVA", outageBusId = None, listOfTransientBranches = []):
#outageBusId = busid of the bus nodal exposure calculated for 
    muCaseFormat = "miso-se.xt"
    if isoStr == "MISO": muCaseFormat = "miso-se.xt"  
    
    transientBranches = ','.join(listOfTransientBranches) 
    
    urlTmp = "https://api1.marginalunit.com/reflow/{muCaseFormat}/{caseName}/node_exposure/bus"
    url = urlTmp.format(muCaseFormat = muCaseFormat, caseName = caseName)
    
    params = {"source":outageBusId, "energized": transientBranches}
    logging.debug("Downloading GSF for bus {0}".format(outageBusId))
    df = _fetch_dfm_post(url, params)

    df = df.rename(columns={"exposure":outageBusId})
    logging.debug(df.head())   
    return df   

def getListOfBindingConstraints(isoStr = "MISO", marketStr = 'da', startDate = arrow.get("2018-01-01"), endDate = arrow.now()):
    
    startDateStr = startDate.format("YYYY-MM-DD")
    endDateStr = endDate.format("YYYY-MM-DD")
    urlTmp = "https://api1.marginalunit.com/misc-data/{isoStr}/binding_constraints?market={marketStr}&start={startDateStr}&end={endDateStr}"
    url = urlTmp.format(isoStr = isoStr.lower(), marketStr = marketStr.lower(), startDateStr = startDateStr, endDateStr = endDateStr)
    
    logging.debug(url)
    
    resp = requests.get(url,auth=REFLOW_AUTH)
    df = pd.read_csv(io.StringIO(resp.text))
        
    logging.debug(df.head())   
    return df    
   
@memoize(maxsize=10000)  
def getCaseLODFsForListOfOutages2(isoStr = "MISO", caseName = "miso_se_20210309-1800_AREVA", outageMemo = '02E-CL2        1', listOfTransientBranchesStr = ''):
#Getting LODFs for bucket of outages
#MN need to be tested
    
    muCaseFormat = "miso-se.xt"
    if isoStr == "MISO": muCaseFormat = "miso-se.xt"  
    
    transientBranches = listOfTransientBranchesStr
    outagesBranches = outageMemo

    urlTmp = "https://api1.marginalunit.com/reflow/{muCaseFormat}/{caseName}/lodf"
    url = urlTmp.format(muCaseFormat = muCaseFormat, caseName = caseName)
    
    params = {"outaged": outagesBranches, "energized": transientBranches} 
    logging.debug("Downloading LODF for {0}".format(outagesBranches))
    
    df = _fetch_dfm_post(url, params)
    df = df.where(pd.notnull(df), None)

    logging.debug(df.head())   
    return df   

def getCaseLODFsForListOfOutages(isoStr = "MISO", caseName = "miso_se_20210309-1800_AREVA", listOfOutages = ['02E-CL2        1'], listOfTransientBranches = []):
#Getting LODFs for bucket of outages
#MN need to be tested
    
    muCaseFormat = "miso-se.xt"
    if isoStr == "MISO": muCaseFormat = "miso-se.xt"  
    
    transientBranches = ','.join(listOfTransientBranches)
    outagesBranches = ','.join(listOfOutages)

    urlTmp = "https://api1.marginalunit.com/reflow/{muCaseFormat}/{caseName}/lodf"
    url = urlTmp.format(muCaseFormat = muCaseFormat, caseName = caseName)
    
    params = {"outaged": outagesBranches, "energized": transientBranches} 
    logging.debug("Downloading LODF for {0}".format(outagesBranches))
    df = _fetch_dfm_post(url, params)
    df = df.where(pd.notnull(df), None)

    logging.debug(df.head())   
    return df   

def getCaseTransientBranches(isoStr = "MISO", caseName = "miso_se_20210309-1800_AREVA"):
#Find Transient Outages in Latest Case and create a single string variable with comma delimited
#If last_closed populated, those are TRANSIENT outages and need to be energized  (not doing this here)    
    #Default is MISO
    muCaseFormat = "miso-se.xt"
    if isoStr == "MISO": muCaseFormat = "miso-se.xt"          
        
    urlTmp="https://api1.marginalunit.com/reflow/{muCaseFormat}/{caseName}/branches?columns=name,last_closed"
    url = urlTmp.format(muCaseFormat = muCaseFormat, caseName = caseName)
    
    resp = requests.get(url,auth=REFLOW_AUTH)
    df = pd.read_csv(io.StringIO(resp.text))
    df = df.dropna()
        
    #Out of these branches, which ones are out in the REFERENCE case and only energize those
    df1 = getCaseBranches(isoStr = isoStr, caseName = caseName)
    df1 = df1[df1['status']==False]['name']
    
    df = df[df['name'].isin(df1)]
               
    logging.debug(df.head())   
    return df    


@memoize(maxsize=10000)  
def getBranchPreOutageFlowsOverDateRange2(isoStr = "MISO", startDate= arrow.now().shift(years=-1), endDate= arrow.now(), branchMemo = '93505          1'):
#GET MAX ABS FLOW from PREVIOUS YEAR SAME DELIVERY MONTH TO CURRENT
#THIS WAS CHANGED AS T ERROR PRONE AS SOME BRANCHES CAN BE ON OUTAGE AN ENTIRE MONTH

    #Default is MISO
    muCaseFormat = "miso-se.xt"
    if isoStr == "MISO": muCaseFormat = "miso-se.xt"   
    startDateStr = startDate.format("YYYYMMDD")   
    endDateStr = endDate.format("YYYYMMDD")   

    urlTmp = "https://api1.marginalunit.com/reflow/{muCaseFormat}/branch?name={branchMemo}&from_case=miso_se_{startDateStr}-1800_AREVA&to_case=miso_se_{endDateStr}-1800_AREVA"
    url = urlTmp.format(muCaseFormat = muCaseFormat, startDateStr = startDateStr, endDateStr = endDateStr, branchMemo = branchMemo)
    
    resp = requests.get(url,auth=REFLOW_AUTH)
    df = pd.read_csv(io.StringIO(resp.text))
    
    logging.debug(df.head())
    if df.empty:
        preOutageFlow = 0
    else:
        #ABS ensures we are getting max from both sizes of the zero line
        #We are not doing MAX flow hence below is commented out...we are doing TWO STD DEV
        try:
            df = df[df['flow'] != 0]
            preOutageFlow = abs(df["flow"]).quantile(0.95)
        except:
            preOutageFlow = 0
            
    if isoStr == "MISO":
        df['MARKETDATE'] = df.apply(lambda row: arrow.get(str(row['case'].replace('miso_se_','').replace('_AREVA','')).split('-')[0]).format('YYYY-MM-DD'), axis = 1)
        df['HOUR'] = df.apply(lambda row: int(int(str(row['case'].replace('miso_se_','').replace('_AREVA','')).split('-')[1])/100), axis = 1)
        df['HE'] = df['HOUR'] + 1
        df['MARKETDATE'] = pd.to_datetime(df['MARKETDATE'])
    logging.debug("2std preoutage flows measured from {0} to {1}: {2}".format(startDateStr,endDateStr,preOutageFlow))
    return df

@memoize(maxsize=10000)  
def getBranchPreOutageFlowsOverDateRange(isoStr = "MISO", startDate= arrow.now().shift(years=-1), endDate= arrow.now(), branchMemo = '93505          1'):
#GET MAX ABS FLOW from PREVIOUS YEAR SAME DELIVERY MONTH TO CURRENT
#THIS WAS CHANGED AS T ERROR PRONE AS SOME BRANCHES CAN BE ON OUTAGE AN ENTIRE MONTH

    #Default is MISO
    muCaseFormat = "miso-se.xt"
    if isoStr == "MISO": muCaseFormat = "miso-se.xt"   
    startDateStr = startDate.format("YYYYMMDD")   
    endDateStr = endDate.format("YYYYMMDD")   

    urlTmp = "https://api1.marginalunit.com/reflow/{muCaseFormat}/branch?name={branchMemo}&from_case=miso_se_{startDateStr}-1800_AREVA&to_case=miso_se_{endDateStr}-1800_AREVA"
    url = urlTmp.format(muCaseFormat = muCaseFormat, startDateStr = startDateStr, endDateStr = endDateStr, branchMemo = branchMemo)
    
    resp = requests.get(url,auth=REFLOW_AUTH)
    df = pd.read_csv(io.StringIO(resp.text))
      
    logging.debug(df.head())
    if df.empty:
        preOutageFlow = 0
    else:
        try:
            df = df[df['flow'] != 0]
            preOutageFlow = abs(df["flow"]).quantile(0.95)
        except:
            preOutageFlow = 0

    logging.debug("2std preoutage flows measured from {0} to {1}: {2}".format(startDateStr,endDateStr,preOutageFlow))
    return preOutageFlow
 
def getCaseGenData(isoStr = "MISO", caseName = "miso_se_20210309-1800_AREVA"):
#Get Gen Data for latest Case [this is for Gen Outage data as we need the PMAX of all the units on outage]
    
        #Default is MISO
        muCaseFormat = "miso-se.xt"
        if isoStr == "MISO": muCaseFormat = "miso-se.xt"         

        urlTmp = "https://api1.marginalunit.com/reflow/{muCaseFormat}/{caseName}/generators"
        url = urlTmp.format(muCaseFormat = muCaseFormat, caseName = caseName)
    
        resp = requests.get(url,auth=REFLOW_AUTH)
        df = pd.read_csv(io.StringIO(resp.text))
        
        logging.debug(df.head())   
#MMN This is to be further joined against the list of gen that is expected to be on outage        
        return df
        
 
def getCaseBusCoordinates(isoStr = "MISO", caseName = "miso_se_20210309-1800_AREVA"):

    #Default is MISO
    muCaseFormat = "miso-se"
    if isoStr == "MISO": muCaseFormat = "miso-se"    

    #Get bus coordinates
    urlTmp="https://api1.marginalunit.com/rms/miso/bus-coordinates/{muCaseFormat}/{caseName}/bus-mappings"
    url = urlTmp.format(muCaseFormat = muCaseFormat, caseName = caseName)
    
    resp = requests.get(url,auth=REFLOW_AUTH)
    df = pd.read_csv(io.StringIO(resp.text))
    logging.debug(df.head())   
#MN I am sure if I need to check if coordinates are there since I got the latest case name fromthe
#bus-coordinates endpoint    
    return df

@retry(Error,tries=3, delay=3, backoff=2)
def getCaseBranches(isoStr = "MISO", caseName = "miso_se_20210309-1800_AREVA"):
    
    #Default is MISO
    muCaseFormat = "miso-se.xt"
    if isoStr == "MISO": muCaseFormat = "miso-se.xt"
    #if isoStr == "MISO": muCaseFormat = "miso-se"
            
    #Get Branches From Latest Case
    urlTmp = "https://api1.marginalunit.com/reflow/{muCaseFormat}/{caseName}/branches"
    url = urlTmp.format(muCaseFormat = muCaseFormat, caseName = caseName)
    
    resp = requests.get(url,auth=REFLOW_AUTH)
    
    try:
        df = pd.read_csv(io.StringIO(resp.text))
        #Remove white spaces
        df['memo_nospaces'] = df['name'].str.replace(' ','')    
        #Some SPP branches have names that are being duplicated. We will remove them. 
        #THIS ENSURES NO DUPLICATES i.e. each outage will return only ONE branch equiv
        df = df.drop_duplicates(subset = 'memo_nospaces',keep=False)
    except:
        print("Fail on url: {0}".format(url))
        raise Error()
    
    logging.debug(df.head())
    return df
  
def getCaseNameForDate(isoStr = "MISO", aDate= arrow.now().shift(years=-1), aHour = 18):
#get case name for date

    #Default is MISO
    muCaseFormat = "miso_se"
    if isoStr == "MISO": muCaseFormat = "miso_se"   
    aDateStr = aDate.format("YYYYMMDD")   
    aHourStr = "{:02d}".format(aHour)
    
    caseName = "{muCaseFormat}_{aDateStr}-{aHourStr}00_AREVA".format(muCaseFormat = muCaseFormat, aDateStr = aDateStr, aHourStr = aHourStr)
    
    return caseName

def getTransmissionOutagesForDateMU(isoStr = 'MISO', reportDate = arrow.now(), aDate = arrow.get('2021-07-25'), caseName = "miso_se_20210309-1800_AREVA"):
    
    aDateStr = aDate.format("YYYY-MM-DD")
    
    if isoStr == 'MISO':
        #MN need to combine outages from MISO, SPP, PJM
        misodf = getTransmissionOutages(isoStr = isoStr, aDate = reportDate)
        #print(misodf.head())
        misomisomapdf = getOutageMappingToOtherIso(isoStr = "MISO", isoStrNeighbour = "MISO", caseName = caseName)
        misodf = misodf.merge(misomisomapdf, how='left', left_on = ['FACILITY_UID'], right_on = ['FACILITY_UID'])
        #misodf['MONITORED_ELEMENT'] = misodf.apply(lambda row: str(row['EMS_EQUIPMENT_NAME']) + str(row['EMS_KEY']),axis = 1)
        misodf = misodf[['OUTAGE_ISO','TICKETID','PLANNED_STARTDATE','PLANNED_ENDDATE','LASTCHANGEDATE','VOLTAGE','MONITORED_ELEMENT','FACILITY_UID']]
        
        sppdf = getTransmissionOutages(isoStr = "SPP", aDate = reportDate)
        #print(sppdf.head())
        sppmisomapdf = getOutageMappingToOtherIso(isoStr = "MISO", isoStrNeighbour = "SPP", caseName = caseName)
        sppdf = sppdf.merge(sppmisomapdf, how='left', left_on = ['FACILITY_UID'], right_on = ['FACILITY_UID'])
        sppdf = sppdf[['OUTAGE_ISO','TICKETID','PLANNED_STARTDATE','PLANNED_ENDDATE','LASTCHANGEDATE','VOLTAGE','MONITORED_ELEMENT','FACILITY_UID']]
        
        pjmdf = getTransmissionOutages(isoStr = "PJM", aDate = reportDate)
        #print(pjmdf.head())
        pjmmisomapdf = getOutageMappingToOtherIso(isoStr = "MISO", isoStrNeighbour = "PJM", caseName = caseName)
        pjmdf = pjmdf.merge(pjmmisomapdf, how='left', left_on = ['FACILITY_UID'], right_on = ['FACILITY_UID'])
        pjmdf = pjmdf[['OUTAGE_ISO','TICKETID','PLANNED_STARTDATE','PLANNED_ENDDATE','LASTCHANGEDATE','VOLTAGE','MONITORED_ELEMENT','FACILITY_UID']]       

        df = pd.concat([misodf,sppdf,pjmdf])   
        
    if isoStr == 'SPP':
        sppdf = getTransmissionOutages(isoStr = "SPP", aDate = reportDate)
        #print(sppdf.head())
        sppmisomapdf = getOutageMappingToOtherIso(isoStr = "SPP", isoStrNeighbour = "SPP", caseName = caseName)
        sppdf = sppdf.merge(sppmisomapdf, how='left', left_on = ['FACILITY_UID'], right_on = ['FACILITY_UID'])
        sppdf = sppdf[['OUTAGE_ISO','TICKETID','PLANNED_STARTDATE','PLANNED_ENDDATE','LASTCHANGEDATE','VOLTAGE','MONITORED_ELEMENT','FACILITY_UID']]

        misodf = getTransmissionOutages(isoStr = "MISO", aDate = reportDate)
        #print(misodf.head())
        misosppmapdf = getOutageMappingToOtherIso(isoStr = "SPP", isoStrNeighbour = "MISO", caseName = caseName)
        misodf = misodf.merge(misosppmapdf, how='left', left_on = ['FACILITY_UID'], right_on = ['FACILITY_UID'])
        misodf = misodf[['OUTAGE_ISO','TICKETID','PLANNED_STARTDATE','PLANNED_ENDDATE','LASTCHANGEDATE','VOLTAGE','MONITORED_ELEMENT','FACILITY_UID']]
                
        df = pd.concat([sppdf,misodf])   
     
    #MN apply date mask to select only outages for the flow dateyes
    mask1 = (df['PLANNED_STARTDATE'] <= aDateStr)
    mask2 = (df['PLANNED_ENDDATE'] >= aDateStr)
    mask = (mask1 & mask2)
    daily_df = df[mask]  
    
    return daily_df

def getTransmissionOutages(isoStr = 'MISO', aDate = arrow.now()):
    
    #Default is MISO
    aDate.replace(hour=18).replace(minute=0).replace(second=0)
    aDateStr = aDate.format("YYYY-MM-DD HH:mm:ss")  
    
    urlTmp = 'https://api1.marginalunit.com/transoutdb/{isoStr}/planned?report_datetime={aDateStr}&projected_outages_only=true'
    url = urlTmp.format(isoStr = isoStr.lower(), aDateStr = aDateStr)
    
    resp = requests.get(url,auth=REFLOW_AUTH)
    try:
        df = pd.read_csv(io.StringIO(resp.text))
        #print(df.columns)
        #df = pd.read_csv(io.StringIO(resp.text), parse_dates=(['planned_start','planned_end','report_start_datetime','report_end_datetime']))
        #print(df.head())
        if isoStr == 'MISO':
            df = df.rename(columns = {'outage_request_id':'ticketid'})
        if isoStr == 'SPP':
            df = df.rename(columns = {'crow_id':'ticketid'})
        if isoStr == 'PJM':
            df = df.rename(columns = {'ticket':'ticketid'})
       
        df['ticketid'] = df.apply(lambda row: str(row['ticketid']).replace('-',''), axis = 1)
        #print(df.head())

        
        if isoStr == 'MISO':
            df = df[['ticketid','planned_start','planned_end','report_start_datetime','kv','facility_uid','ems_equipment_name','ems_key','equipment_type','common_name','is_branch','notes']]
            #df['mon_elem'] = df.apply(lambda row: str(row['ems_equipment_name']) + str(row['ems_key']),axis = 1)
            df = df.rename(columns={'mon_elem':'MONITORED_ELEMENT','report_start_datetime':'LASTCHANGEDATE','kv':'VOLTAGE',
                                'planned_start':'PLANNED_STARTDATE','planned_end':'PLANNED_ENDDATE'})
            df['OUTAGE_ISO'] = 'MISO'
        if isoStr == 'SPP':
            df = df[['ticketid','planned_start','planned_end','report_start_datetime','rating','is_branch','facility_uid']]
            df = df.rename(columns={'report_start_datetime':'LASTCHANGEDATE','rating':'VOLTAGE','planned_start':'PLANNED_STARTDATE','planned_end':'PLANNED_ENDDATE'})        
            df['OUTAGE_ISO'] = 'SPPISO'
        if isoStr == 'PJM':
            df = df[['ticketid','start_date','end_date','report_start_datetime','is_branch','facility_uid']]
            df = df.rename(columns={'report_start_datetime':'LASTCHANGEDATE','start_date':'PLANNED_STARTDATE','end_date':'PLANNED_ENDDATE'})
            df['VOLTAGE'] = None 
            df['OUTAGE_ISO'] = 'PJMISO'
            
        df.columns = [x.upper() for x in df.columns]    
        #MN only leave branches
        df = df[df['IS_BRANCH']]
         
    except:
        print("Fail on url: {0}".format(url))
        raise Error()
        
    return df

def getOutageMappingToOtherIso(isoStr = "MISO", isoStrNeighbour = "SPP", caseName = "miso_se_20210309-1800_AREVA"):
    
    if isoStrNeighbour == "MISO" and isoStr == "SPP": 
        muCaseFormatNeighbour = "miso" 
        muCaseFormat = "spp-se"
    if isoStrNeighbour == "SPP" and isoStr == "MISO": 
        muCaseFormatNeighbour = "spp" 
        muCaseFormat = "miso-se"
    if isoStrNeighbour == "PJM" and isoStr == "MISO": 
        muCaseFormatNeighbour = "pjm"
        muCaseFormat = "miso-se"
    if isoStrNeighbour == "SPP" and isoStr == "SPP": 
        muCaseFormatNeighbour = "spp" 
        muCaseFormat = "spp-se"
    if isoStrNeighbour == "MISO" and isoStr == "MISO": 
        muCaseFormatNeighbour = "miso" 
        muCaseFormat = "miso-se"
        
    urlTmp = "https://api1.marginalunit.com/rms/{muCaseFormatNeighbour}/transmission-outages/{muCaseFormat}/{caseName}/branch-mappings"
    url = urlTmp.format(muCaseFormat  = muCaseFormat , muCaseFormatNeighbour = muCaseFormatNeighbour, caseName = caseName)
    
    resp = requests.get(url,auth=REFLOW_AUTH)
    
    try:
        df = pd.read_csv(io.StringIO(resp.text))
        #print (df.head())
        if isoStrNeighbour == "PJM":
            df = df.rename(columns={"reflow_name":"branch_name"})
    except:
        print("Fail on url: {0}".format(url))
        raise Error()
    
    logging.debug(df.head())
    df = df.rename(columns = {"branch_name":"MONITORED_ELEMENT"})
    df.columns = [x.upper() for x in df.columns]    
    return df[["FACILITY_UID","MONITORED_ELEMENT"]]

  
def getLatestCaseWithCoordinates(isoStr = "MISO"):
#Query which cases we have coordinates for:
    #Default is MISO
    muCaseFormat = "miso-se"
    if isoStr == "MISO": muCaseFormat = "miso-se"
    if isoStr == "SPP": muCaseFormat = "spp-se"
    
    urlTmp = "https://api1.marginalunit.com/rms/{isoStr}/bus-coordinates/{muCaseFormat}/cases"
    url = urlTmp.format(isoStr = isoStr.lower(), muCaseFormat = muCaseFormat)
        
    df = _get_dataframe(url = url, auth = RMS_AUTH)
    logging.debug(df.tail())
    #Latest case with coords
    caseName = df['cases'].iloc[-1]
    logging.debug("Latest case with coordinates: {0}".format(str(caseName)))
    return caseName

#Get Datafram from MU
@retry(RequestError, tries=10, delay = 5, backoff=2)
def _get_dataframe(url, auth):    

#    mu_get_sleep_time = 0
    try:
        resp = requests.get(url, auth = auth)
    except:
        raise RequestError()
        
    if resp.status_code != 200:
        print(resp.text)
    if resp.status_code == 429:
        raise TooManyRequests()
    if resp.status_code == 422:
        raise UnprocessableEntity()
    if resp.status_code == 502:
        raise BadGateway()
    if resp.status_code == 500:
        raise InternalServerError()
        
    resp.raise_for_status()
    df = pd.read_csv(io.StringIO(resp.text))
    return df        

#JSON Object for Large API parameters
@retry(RequestError, tries=5, delay = 5, backoff=2)
def _fetch_dfm_post(url, body_params = None, auth = REFLOW_AUTH):
    #print(auth)
    global mu_post_sleep_time
    global last_call_time
    time.sleep(mu_post_sleep_time)          
    df=None
    
    startcall = datetime.datetime.now()
    try:        
        resp = requests.post(url, data = body_params, auth = auth, headers = {"content-type": "application/x-www-form-urlencoded"})
    except:
        raise RequestError()

    if resp.status_code != 200:
        print(resp.text)
        print("wait time: {0}".format(mu_post_sleep_time))
    if resp.status_code == 500:
        raise InternalServerError()
    if resp.status_code == 429:

        parsed_html = BeautifulSoup(resp.text,features='lxml')
        resptext = parsed_html.body.find('p').text
        limit = int(resptext.replace(' per 1 minute',''))
        if limit < 60.0/(last_call_time+0.05):
            if mu_post_sleep_time < 15:
                mu_post_sleep_time = mu_post_sleep_time + 1    
        raise TooManyRequests()
    if resp.status_code == 422:
        raise UnprocessableEntity()
    if resp.status_code == 502:
        raise BadGateway()
        
    if(mu_post_sleep_time > 1.5):    
        mu_post_sleep_time = mu_post_sleep_time - 0.5   
      
    df = pd.read_csv(io.StringIO(resp.text))
    endcall = datetime.datetime.now()
    calltime = (endcall - startcall).total_seconds()
    last_call_time = calltime

    return df

@retry(RequestError, tries=5, delay = 5, backoff=2)
def try_retry():
    
    for x in range(3):
        print(x)
        time.sleep(1)
    raise RequestError()   
    

if __name__ == '__main__':
    
    logging.basicConfig(format = '%(asctime)s %(levelname)s %(filename)s %(funcName)s %(message)s', level = logging.DEBUG)  

    caseName = getLatestCaseWithCoordinates(isoStr = "SPP")
    df = getTransmissionOutagesForDateMU(isoStr = 'SPP', reportDate = arrow.now(), aDate = arrow.get('2022-01-01'), caseName = caseName)

