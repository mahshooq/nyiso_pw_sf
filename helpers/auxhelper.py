import sys
import os
import numpy as np
import subprocess
import argparse
import shutil
import glob
import decimal
import pandas as pd
pd.options.display.max_columns = None

sys.path.append('C:\\Users\\MAHSHOOQ.MAJEED\\OneDrive - BioUrja Trading LLC\\Documents\\PWSFX\\NYISO_PWSFX_TEST\\pwsfx\\exceptions')
sys.path.append('C:\\Users\\MAHSHOOQ.MAJEED\\OneDrive - BioUrja Trading LLC\\Documents\\PWSFX\\NYISO_PWSFX_TEST\\pwsfx\\config')
sys.path.append('C:\\Users\\MAHSHOOQ.MAJEED\\OneDrive - BioUrja Trading LLC\\Documents\\PWSFX\\NYISO_PWSFX_TEST\\pwsfx\\helpers\\outagehelper')
sys.path.append('C:\\Users\\MAHSHOOQ.MAJEED\\OneDrive - BioUrja Trading LLC\\Documents\\PWSFX\\NYISO_PWSFX_TEST\\pwsfx\\db')

from exceptions import *
from config import connData as cd
from config import globalSettings as gs
from db import create_snowflake_writer_engine
from db import create_snowflake_reader_engine

from outagehelper import generateDateList

def add_runid(filename, runid):
    
    filenameNoExt, filenameExt = os.path.splitext(filename)
    return filenameNoExt+'_runid'+str(runid)+filenameExt

def generateAuxInterfaceElements(df, outputDir, outputAuxFile, id_field = 'INTERFACE_NAME', dbstr = 'snowflakedb'):

    auxHeader = '''
//DEFINE INTERFACE ELEMENTS
DATA (InterfaceElement, [InterfaceName,Element,MeterFar,Weight],,YES)
{
'''
    auxFooter = '''}'''     
    record_list = df.to_dict(orient='records')
    auxBody = ''    
    for x in record_list:
        interfacename = x[id_field]
        if x['ELEMENTTYPE'] == 'MONITORED':
            meterfar = '"NO"'
            weight = x['WEIGHT']
            memo = "BRANCH "+"'"+x['BRANCH_MEMO']+"'"
        else:
            meterfar = '""'
            weight = '""'
            memo = "BRANCHOPEN "+"'"+x['BRANCH_MEMO']+"'"
            
        recordStr = ' '.join(['"'+str(interfacename)+'"','"'+str(memo)+'"',str(meterfar),str(weight)])+"\n"
        auxBody = auxBody + recordStr

    auxInterfaceSelect = '''

//SELECT ALL INTERFACES
SCRIPT
{
SelectAll(Interface, "");
}
'''
        
    auxStr = auxHeader + auxBody + auxFooter + auxInterfaceSelect

    outputAuxFileFullPath = fullPath(outputDir,outputAuxFile)
    
    #with open(outputAuxFileFullPath, 'w') as file:
    #    file.write(auxStr)   
    
    return auxStr

def calcTLR(isoStr,runid = 0):
        
    auxStrTmp = '''
//CALCULATE TLRs
SCRIPT
{{
//SolvePowerFlow(DC);
CalculateTLRMultipleElement(Interface,Selected,Buyer,[InjectionGroup "{isoStr}.TOTAL.LOAD"],DC);
SaveData("MultBusTLRResults_runid{runid}.csv",CSVColHeader,InjectionGroup,[InjGrpName,ETLR,WTLR,MultBusTLRSens:ALL],[]);
}}
'''

    auxStr = auxStrTmp.format(isoStr=isoStr, runid=runid)
    return auxStr

def saveCase(isoStr, outputDir, modelFile, runid = 0):
        
    newModelFile = add_runid(modelFile, runid)
    filenameNoExt, filenameExt = os.path.splitext(newModelFile)
    newModelFile = filenameNoExt + '.pwb'
    
    auxStrTmp = '''
//SAVE CASE
SCRIPT
{{
SetCurrentDirectory("{outputdir}", NO);    
SaveCase({casefile});
}}
'''

    auxStr = auxStrTmp.format(outputdir=outputDir, casefile=newModelFile)
    return auxStr

def saveCaseAndExit(isoStr, outputDir, modelFile, runid = 0):
        
    newModelFile = add_runid(modelFIle, runid)
    filenameNoExt, filenameExt = os.path.splitext(newModelFile)
    newModelFile = filenameNoExt + '.pwb'

    auxStrTmp = '''
//SAVE CASE AND EXIT
SCRIPT
{{
SetCurrentDirectory("{outputdir}", NO);    
SaveCase({casefile});
ExitProgram;
}}
'''

    auxStr = auxStrTmp.format(outputdir=outputDir, casefile=newModelFile)
    return auxStr

def exitPowerWorld():
        
    auxStrTmp = '''
//EXIT
SCRIPT
{{
ExitProgram;
}}
'''

    auxStr = auxStrTmp.format(0)
    return auxStr

def generateAuxInjectionGroups(isoStr,outputDir, outputAuxFile, dbstr = 'snowflakedb'):
    
    sqlStrTmp = '''       
    SELECT OBJECT, CONTAINEDBY, PARFAC FROM POWERDB_BIZDEV.PTEST.PWSFX_{isoStr}_INJGRUOPS
    '''
    
    sqlStr =  sqlStrTmp.format(isoStr=isoStr)
    
    df = None
    db_engine = create_snowflake_reader_engine(account=cd[dbstr]['account'],user=cd[dbstr]['user'],
                                               password=cd[dbstr]['password'],role=cd[dbstr]['role'],
                                               warehouse=cd[dbstr]['warehouse'],database=cd[dbstr]['database'])
    con = db_engine.connect()

    try:
        df = pd.read_sql(sqlStr, con = con, index_col = None)
    except:
        import sys, traceback
        exc_type, exc_value, exc_traceback = sys.exc_info()
        print("-"*60)
        print("*** print_tb:")
        traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
        print("*** print_exception:")
        traceback.print_exception(exc_type, exc_value, exc_traceback,
                                  limit=2, file=sys.stdout, chain=True)
        print("-"*60)
        raise
    finally:
        con.close()
        db_engine.dispose()

    df.columns = [x.upper() for x in df.columns]
    
    record_list = df.to_dict(orient='records')

    auxHeader = '''
//DEFINE INJECTION GROUPS
DATA (PartPoint, [Object,GroupName,AutoCalcMethod,PartFact,AutoCalc],,YES)
{
'''
    auxFooter = '''}'''   
    auxBody = ''    
    for x in record_list:
        recordStr = ' '.join(['"'+str(x['OBJECT'])+'"','"'+str(x['CONTAINEDBY'])+'"','"SPECIFIED"',str(x['PARFAC']),'"NO"'])+"\n"
        auxBody = auxBody + recordStr
        
    auxStr = auxHeader + auxBody + auxFooter

    outputAuxFileFullPath = fullPath(outputDir,outputAuxFile)
        
    #with open(outputAuxFileFullPath, 'w') as file:
    #    file.write(auxStr)   
    
    return auxStr


def fullPath(dirname, filename):

    fileFullPath = "\\".join([dirname,filename])
    fileFullPath = fileFullPath.replace('\\','//')
    fileFullPath = fileFullPath.replace('///','//')
    fileFullPath = fileFullPath.replace('//','\\')

    return fileFullPath

def getISOInterfaces(dbstr = 'snowflakedb', iso = "MISO", startdate = '2022-01-01',enddate='2022-01-31'):

    if iso == 'MISO':
        sqlStrTmp = '''       
WITH UOC AS (
SELECT BU_ISO, BU_CID, CID, FULL_INTERFACE_NAME, CONSTRAINT_NAME, 
CONTINGENCY_UID, MONEL_MEMO, CONVENTION, FLOW_STARTDATE, 
FLOW_ENDDATE, INSERT_DATE
FROM POWERDB_BIZDEV.PTEST.PWSFX_UOC 
WHERE BU_ISO = '{iso}' AND FLOW_STARTDATE = '{startdate}' AND FLOW_ENDDATE = '{enddate}'
AND BU_CID != 3003974 AND 
INSERT_DATE = (SELECT MAX(INSERT_DATE) FROM POWERDB_BIZDEV.PTEST.PWSFX_UOC WHERE BU_ISO = '{iso}' 
AND FLOW_STARTDATE = '{startdate}' AND FLOW_ENDDATE = '{enddate}')), 
-- NEED TO FIND OUT WHAT IS GOING ON WITH THIS "JEFF2-WOODY FLO HUGH-ZACHARY"
CTGS AS (SELECT * FROM POWERDB_BIZDEV.PTEST.PWSFX_MISO_UOC_CTGS_VW)
SELECT uoc.BU_CID, uoc.FULL_INTERFACE_NAME AS INTERFACE_NAME, uoc.MONEL_MEMO AS BRANCH_MEMO, 
uoc.CONVENTION AS WEIGHT, 'MONITORED' AS ELEMENTTYPE, CONCAT('BRANCH',' ',br.BUSNUMFROM,' ',br.BUSNUMTO,' ',br.CIRCUIT) AS COMMENT
FROM UOC uoc, POWERDB_BIZDEV.PTEST.PWSFX_MISO_PWBRANCHES br
WHERE uoc.MONEL_MEMO = br.CLEANMEMO 
UNION
SELECT uoc.BU_CID, uoc.FULL_INTERFACE_NAME AS INTERFACE_NAME, ctgs.DEVICENAME AS BRANCH_MEMO, 
NULL AS WEIGHT, 'CONTINGENT' AS ELEMENTTYPE, CONCAT('BRANCHOPEN',' ',ctgs.BUSNUMFROM,' ',ctgs.BUSNUMTO,' ',ctgs.CIRCUIT) AS COMMENT 
FROM UOC uoc, CTGS ctgs
WHERE uoc.CONTINGENCY_UID = ctgs.CTGID
ORDER BY INTERFACE_NAME, ELEMENTTYPE DESC
        '''

    sqlStr = sqlStrTmp.format(iso=iso,startdate=startdate,enddate=enddate)
    
    df = None
    db_engine = create_snowflake_reader_engine(account=cd[dbstr]['account'],user=cd[dbstr]['user'],
                                               password=cd[dbstr]['password'],role=cd[dbstr]['role'],
                                               warehouse=cd[dbstr]['warehouse'],database=cd[dbstr]['database'])
    con = db_engine.connect()
        
    try:
        df = pd.read_sql(sqlStr, con = con, index_col = None)
    except:
        import sys, traceback
        exc_type, exc_value, exc_traceback = sys.exc_info()
        print("-"*60)
        print("*** print_tb:")
        traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
        print("*** print_exception:")
        traceback.print_exception(exc_type, exc_value, exc_traceback,
                                  limit=2, file=sys.stdout, chain=True)
        print("-"*60)
        raise
    finally:
        con.close()
        db_engine.dispose()

    df.columns = [x.upper() for x in df.columns]
    df = df.astype(object).replace(np.nan, '')
    try:
        df['INTERFACE_NAME'] = df['INTERFACE_NAME'].astype('str')
        df['BRANCH_MEMO'] = df['BRANCH_MEMO'].astype('str')
        df['WEIGHT'] = df['WEIGHT'].astype('str')
        df['ELEMENTTYPE'] = df['ELEMENTTYPE'].astype('str')
        df['COMMENT'] = df['COMMENT'].astype('str')
    except:
        #pass
        raise
    
    return df

def generateAuxOpenPlannedOutages(outputDir, outputAuxFile, dateStr = '2021-02-01', isoStr = 'MISO', dbstr = 'snowflakedb'):

    sqlStrTmp = '''       
    SELECT otg.ISO, otg.OUTAGE_ISO, otg.FACILITY_UID, otg.PLANNED_STARTDATE, 
    otg.PLANNED_ENDDATE, otg.MONITORED_ELEMENT, br.BUSNUMFROM , br.BUSNUMTO, br.CIRCUIT 
    FROM POWERDB_BIZDEV.PTEST.PWSFX_{isoStr}_PLANNEDOUTAGES otg
    LEFT JOIN
    POWERDB_BIZDEV.PTEST.PWSFX_{isoStr}_PWBRANCHES br ON
    otg.MONITORED_ELEMENT = br.CLEANMEMO
    WHERE MONITORED_ELEMENT IS NOT NULL
    AND otg.TARGET_DATE = '{dateStr}' AND ISO = '{isoStr}'
    ORDER BY otg.MONITORED_ELEMENT DESC, otg.OUTAGE_ISO
    '''
    
    sqlStr =  sqlStrTmp.format(dateStr = dateStr, isoStr = isoStr)
    
    df = None
    db_engine = create_snowflake_reader_engine(account=cd[dbstr]['account'],user=cd[dbstr]['user'],
                                               password=cd[dbstr]['password'],role=cd[dbstr]['role'],
                                               warehouse=cd[dbstr]['warehouse'],database=cd[dbstr]['database'])
    con = db_engine.connect()

    try:
        df = pd.read_sql(sqlStr, con = con, index_col = None)
    except:
        import sys, traceback
        exc_type, exc_value, exc_traceback = sys.exc_info()
        print("-"*60)
        print("*** print_tb:")
        traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
        print("*** print_exception:")
        traceback.print_exception(exc_type, exc_value, exc_traceback,
                                  limit=2, file=sys.stdout, chain=True)
        print("-"*60)
        raise
    finally:
        con.close()
        db_engine.dispose()

    df.columns = [x.upper() for x in df.columns]
    
    record_list = df.to_dict(orient='records')
    
    auxHeader = '''
//CHANGE STATUS OF BRANCHES CORRESPONDING TO PLANNED OUTAGES TO OPEN
DATA (BRANCH, [BusNumFrom,BusNumTo,Circuit,Status])
{
'''
    auxFooter = '''
}
'''
    
    auxBody = ''
    for x in record_list:
        recordStr = ' '.join([str(x['BUSNUMFROM']),str(x['BUSNUMTO']),'"'+str(x['CIRCUIT'])+'"','"Open"'])+"\n"
        auxBody = auxBody + recordStr
        
    auxStr = auxHeader + auxBody + auxFooter

    outputAuxFileFullPath = fullPath(outputDir,outputAuxFile)
        
    #with open(outputAuxFileFullPath, 'w') as file:
    #    file.write(auxStr)   
    
    return auxStr

def generateAuxCloseAdditionalModelOutages(outputDir, outputAuxFile, isoStr = 'MISO', dbstr = 'snowflakedb'):

    #There are some outages in the model which are not listed in the outages file from FTR package,
    #they are also not mentioned in the outage plan, we verify if they flow power in the most recent SE case
    #and if they do close them
    if isoStr == 'MISO':
        sqlStrTmp = '''       
SELECT DISTINCT CLEANMEMO, BUSNUMFROM, BUSNUMTO, CIRCUIT FROM
(--SELECT MODEL OPEN BRANHCES
SELECT CLEANMEMO, BUSNUMFROM, BUSNUMTO, CIRCUIT FROM POWERDB_BIZDEV.PTEST.PWSFX_MISO_PWBRANCHES 
WHERE STATUS = 'Open') br,
(--SELECT ONLY ISO-TO-ISO BRANCHES
SELECT DISTINCT RPTG_ISO AS BU_ISO, BRANCH_NAME AS DEVICENAME, FROM_ISO, TO_ISO
FROM POWERDB.PMUSE.SE_BRANCHES 
WHERE RPTG_ISO = '{isoStr}' AND FROM_ISO = TO_ISO 
AND ((FROM_ISO = 'MISO') OR (FROM_ISO = 'PJMISO') OR (FROM_ISO = 'SPPISO'))) iso2iso,
(SELECT BRANCH_NAME, AVG_FLOW FROM POWERDB.PMUSE.SE_BRANCH_FLOWS_HOURLY WHERE 
RPTG_ISO = '{isoStr}' AND MARKET_TYPE = 'RT' AND FLOW_DATE = (SELECT MAX(FLOW_DATE) FROM POWERDB.PMUSE.SE_BRANCH_FLOWS_HOURLY WHERE 
RPTG_ISO = '{isoStr}' AND MARKET_TYPE = 'RT')) flow
WHERE br.CLEANMEMO = iso2iso.DEVICENAME
AND br.CLEANMEMO = flow.BRANCH_NAME
AND br.CLEANMEMO NOT IN
(--CHECK IF OPEN BRANCH IS MODEL OUTAGE
SELECT DISTINCT DEVICENAME FROM POWERDB_BIZDEV.PTEST.PWSFX_MISO_MODELOUTAGES)
AND br.CLEANMEMO NOT IN
(--CHECK IF OPEN BRANCH IS PLANNED OUTAGE
SELECT DISTINCT MONITORED_ELEMENT AS MEMO
FROM POWERDB_BIZDEV.PTEST.PWSFX_MISO_PLANNEDOUTAGES 
WHERE MONITORED_ELEMENT IS NOT NULL)
AND flow.AVG_FLOW !=0
ORDER BY br.CLEANMEMO
'''

    sqlStr =  sqlStrTmp.format(isoStr=isoStr)
    
    df = None
    db_engine = create_snowflake_reader_engine(account=cd[dbstr]['account'],user=cd[dbstr]['user'],
                                               password=cd[dbstr]['password'],role=cd[dbstr]['role'],
                                               warehouse=cd[dbstr]['warehouse'],database=cd[dbstr]['database'])
    con = db_engine.connect()

    try:
        df = pd.read_sql(sqlStr, con = con, index_col = None)
    except:
        import sys, traceback
        exc_type, exc_value, exc_traceback = sys.exc_info()
        print("-"*60)
        print("*** print_tb:")
        traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
        print("*** print_exception:")
        traceback.print_exception(exc_type, exc_value, exc_traceback,
                                  limit=2, file=sys.stdout, chain=True)
        print("-"*60)
        raise
    finally:
        con.close()
        db_engine.dispose()

    df.columns = [x.upper() for x in df.columns]
    
    record_list = df.to_dict(orient='records')
    
    auxHeader = '''
//CHANGE STATUS OF BRANCHES THAT ARE NOT MENTIONED IN THE OUTAGE PLAN, FOR SOME REASON OPEN IN THE MODEL
//BUT FLOW POWER IN THE RECENT SE CASE
DATA (BRANCH, [BusNumFrom,BusNumTo,Circuit,Status])
{
'''
    auxFooter = '''}'''
    
    auxBody = ''
    for x in record_list:
        recordStr = ' '.join([str(x['BUSNUMFROM']),str(x['BUSNUMTO']),'"'+str(x['CIRCUIT'])+'"','"Closed"'])+"\n"
        auxBody = auxBody + recordStr
        
    auxStr = auxHeader + auxBody + auxFooter

    outputAuxFileFullPath = fullPath(outputDir,outputAuxFile)
    
    #with open(outputAuxFileFullPath, 'w') as file:
    #    file.write(auxStr)   

    return auxStr
    
def generateAuxCloseModelOutages(outputDir, outputAuxFile, isoStr = 'MISO', dbstr = 'snowflakedb'):

    #We need to close model outages (transient) but only those that go from ISO to ISO
    #as we will be opening/closing those outages for each analysis day according to the outage plan
    #attribution to the ISO is done using data from POWERDB.PMUSE.SE_BRANCHES 
    if isoStr == 'MISO':
        sqlStrTmp = '''       
SELECT DISTINCT otgs.DEVICENAME, br.BUSNUMFROM, br.BUSNUMTO, br.CIRCUIT
FROM POWERDB_BIZDEV.PTEST.PWSFX_MISO_MODELOUTAGES otgs,
POWERDB_BIZDEV.PTEST.PWSFX_MISO_PWBRANCHES br,
(SELECT DISTINCT RPTG_ISO AS BU_ISO, BRANCH_NAME AS DEVICENAME, FROM_ISO, TO_ISO
FROM POWERDB.PMUSE.SE_BRANCHES 
WHERE RPTG_ISO = 'MISO' AND FROM_ISO = TO_ISO 
AND ((FROM_ISO = 'MISO') OR (FROM_ISO = 'PJMISO') OR (FROM_ISO = 'SPPISO'))) iso2iso
WHERE br.CLEANMEMO = otgs.DEVICENAME AND otgs.DEVICENAME = iso2iso.DEVICENAME
        '''

    if isoStr == 'SPISO':
        sqlStrTmp = '''       
SELECT DISTINCT otgs.DEVICENAME, br.BUSNUMFROM, br.BUSNUMTO, br.CIRCUIT
FROM POWERDB_BIZDEV.PTEST.PWSFX_SPPISO_MODELOUTAGES otgs,
POWERDB_BIZDEV.PTEST.PWSFX_SPPISO_PWBRANCHES br,
(SELECT DISTINCT RPTG_ISO AS BU_ISO, BRANCH_NAME AS DEVICENAME, FROM_ISO, TO_ISO
FROM POWERDB.PMUSE.SE_BRANCHES 
WHERE RPTG_ISO = 'SPPISO' AND FROM_ISO = TO_ISO 
AND ((FROM_ISO = 'MISO') OR (FROM_ISO = 'PJMISO') OR (FROM_ISO = 'SPPISO'))) iso2iso
WHERE br.CLEANMEMO = otgs.DEVICENAME AND otgs.DEVICENAME = iso2iso.DEVICENAME
        '''    
    
    sqlStr =  sqlStrTmp.format(0)
    
    df = None
    db_engine = create_snowflake_reader_engine(account=cd[dbstr]['account'],user=cd[dbstr]['user'],
                                               password=cd[dbstr]['password'],role=cd[dbstr]['role'],
                                               warehouse=cd[dbstr]['warehouse'],database=cd[dbstr]['database'])
    con = db_engine.connect()

    try:
        df = pd.read_sql(sqlStr, con = con, index_col = None)
    except:
        import sys, traceback
        exc_type, exc_value, exc_traceback = sys.exc_info()
        print("-"*60)
        print("*** print_tb:")
        traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
        print("*** print_exception:")
        traceback.print_exception(exc_type, exc_value, exc_traceback,
                                  limit=2, file=sys.stdout, chain=True)
        print("-"*60)
        raise
    finally:
        con.close()
        db_engine.dispose()

    df.columns = [x.upper() for x in df.columns]
    
    record_list = df.to_dict(orient='records')
    
    auxHeader = '''
//CHANGE STATUS OF BRANCHES CORRESPONDING TO TRANSIENT OUTAGES TO CLOSED
DATA (BRANCH, [BusNumFrom,BusNumTo,Circuit,Status])
{
'''
    auxFooter = '''}'''
    
    auxBody = ''
    for x in record_list:
        recordStr = ' '.join([str(x['BUSNUMFROM']),str(x['BUSNUMTO']),'"'+str(x['CIRCUIT'])+'"','"Closed"'])+"\n"
        auxBody = auxBody + recordStr
        
    auxStr = auxHeader + auxBody + auxFooter

    outputAuxFileFullPath = fullPath(outputDir,outputAuxFile)
    
    #with open(outputAuxFileFullPath, 'w') as file:
    #    file.write(auxStr)   

    return auxStr

def openCaseCopy(isoStr, outputDir, modelFile, runid):

    newModelFile = add_runid(modelFile, runid)
    filenameNoExt, filenameExt = os.path.splitext(newModelFile)
    newModelFile = filenameNoExt + '.pwb'
    
    auxStrTmp = '''
//OPEN CASE
SCRIPT
{{
SetCurrentDirectory("{outputdir}", NO);    
CopyFile("{casefile}", "{casecopyfile}");
OpenCase("{casecopyfile}",PTI);
}}
'''
    
    auxStr = auxStrTmp.format(outputdir=outputDir, casefile=modelFile, casecopyfile=newModelFile)

    return auxStr

def calcTLR(isoStr, outputDir, modelFile, runid, outputAuxFile):

    assembleAuxFile = add_runid('assemble_model_interface_names.aux',runid)

    newModelFile = add_runid(modelFile, runid)
    filenameNoExt, filenameExt = os.path.splitext(newModelFile)
    newModelFile = filenameNoExt + '.pwb'

    tlrFile = add_runid('multbustlrs.csv', runid)    

#    //ASSEMBLE MODEL, CALC TLRS, SAVE RESULTS, EXIT 
    auxStrTmp = '''
SCRIPT
{{
SetCurrentDirectory("{outputdir}", NO);    
LoadAux("{assembleFile}");
CalculateTLRMultipleElement(Interface,Selected,Buyer,[InjectionGroup "{isoStr}.TOTAL.LOAD"],DC);
SaveData("{tlrFile}",CSVColHeader,InjectionGroup,[InjGrpName,ETLR,WTLR,MultBusTLRSens:ALL],[]);
ExitProgram;
}}
'''

    auxStr = auxStrTmp.format(isoStr=isoStr, outputdir=outputDir, assembleFile=assembleAuxFile, casefile=newModelFile, tlrFile=tlrFile)

    outputAuxFileFullPath = fullPath(outputDir,outputAuxFile)        
    with open(outputAuxFileFullPath, 'w') as file:
        file.write(auxStr)   
    
    return auxStr


def prepareForDump(isoStr, inputDir, outputDir, modelFile, outputAuxFile):

    auxStrTmp = '''
    //DUMP BUS, BRANCH, GEN, LOAD CASE DATA TO CSV FILES
    SCRIPT
    {{
    SetCurrentDirectory("{outputdir}", NO);
    
    DeleteFile("{pw_branch_dump}");
    DeleteFile("{pw_bus_dump}");
    DeleteFile("{pw_gen_dump}");
    DeleteFile("{pw_load_dump}");

    OpenCase("{casefile}",PTI);

    SaveData("{pw_branch_dump}", CSV, BRANCH, [Memo, BusNumFrom, BusNameFrom, BusNumTo, BusNameTo, Circuit, Status, BranchDeviceType, IsXF, MWMax, MWFrom, MWTo, LimitMVAA, LimitMVAB, NomkVFrom, NomkVTo,AreaNameFrom, AreaNameTo], []);
    SaveData("{pw_bus_dump}", CSV, BUS, [Memo, Number, Name, Slack, Status, AreaName, NomkV, LoadMW, GenMW], []);
    SaveData("{pw_gen_dump}", CSV, GEN, [Memo, BusNum, BusName, ID, Status, MW, AGC, MWMin, MWMax, CostModel, PartFact, BusAreaName, AreaName], []);
    SaveData("{pw_load_dump}", CSV, LOAD, [Memo, BusNum, BusName, ID, Status, BusAreaName, AreaName, MW], []);    
    
    ExitProgram;
    }}
    '''

    auxStr = auxStrTmp.format(outputdir=outputDir, casefile=modelFile,
                              pw_branch_dump = gs[isoStr]['pw_branch_dumpfile'],
                              pw_bus_dump = gs[isoStr]['pw_bus_dumpfile'],
                              pw_gen_dump = gs[isoStr]['pw_gen_dumpfile'],
                              pw_load_dump = gs[isoStr]['pw_load_dumpfile'])

    outputAuxFileFullPath = fullPath(outputDir,outputAuxFile)    
    with open(outputAuxFileFullPath, 'w') as file:
        file.write(auxStr)

    shutil.copy("\\".join([inputDir,modelFile]),outputDir)
        
    return outputAuxFileFullPath 
        
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Dumper.')
    parser.add_argument('--iso', action='store', dest = 'isoStr', type=str, default = 'MISO')
    parser.add_argument('--start', action='store', dest = 'startDate', type=str, default = '2022-02-01')
    parser.add_argument('--end', action='store', dest = 'endDate', type=str, default = '2022-02-28')
    args = parser.parse_args()

    if type(args.startDate) == type(None):
        startdate = gs[args.isoStr]['startdate']
    else:
        startdate = args.startDate
    if type(args.endDate) == type(None):
        enddate = gs[args.isoStr]['enddate']
    else:
        enddate = args.endDate
    
    dbstr = 'snowflakedb'
    db_engine = create_snowflake_writer_engine(
        account = cd[dbstr]['account'],
        user=cd[dbstr]['user'],
        password=cd[dbstr]['password'],
        role=cd[dbstr]['role'],
        warehouse=cd[dbstr]['warehouse'],
        database=cd[dbstr]['database'],
        schema=cd[dbstr]['schema'])
    
    modelFile = 'PSSEv30_Feb22_AUCTION_Feb22Auc-Off-peak.RAW'
  
    outputDir = gs[args.isoStr]['output_dir']
    inputDir = gs[args.isoStr]['input_dir']

    targetdates = generateDateList(startdate = startdate, enddate = enddate)
    runs = ['BASE'] + [x.format('YYYY-MM-DD') for x in targetdates]
    runids = [x for x in enumerate(runs)]
    
    #AUX FILE TO CLOSE TRANSIENT OUTAGES
    close_transient = generateAuxCloseModelOutages(isoStr = args.isoStr, dbstr = 'snowflakedb', outputDir = outputDir, outputAuxFile = 'transientoutagesClose.aux')

    #AUX FILE TO CLOSE OUTAGE THAT ARE NOT PART OF OUTAGE PLAN, OPEN IN THE MODEL, BUT FLOW POWER IN SE
    close_additional =  generateAuxCloseAdditionalModelOutages(outputDir, outputAuxFile = 'additioanloutagesClose.aux', isoStr = 'MISO', dbstr = 'snowflakedb')
    
    #AUX FILE TO CREATE INJECTIONS GROUPS
    inj_groups = generateAuxInjectionGroups(isoStr = args.isoStr, dbstr = 'snowflakedb', outputDir = outputDir, outputAuxFile = 'injectiongroupsCreate.aux')

    #AUX FILES to DEFINE INTERFACES 
    df = getISOInterfaces(dbstr = 'snowflakedb', iso = "MISO", startdate = startdate,enddate=enddate)    
    interface_names = generateAuxInterfaceElements(df, outputDir, outputAuxFile = 'interfacesCreate_names.aux', id_field = 'INTERFACE_NAME',dbstr = 'snowflakedb')
    interface_bucid = generateAuxInterfaceElements(df, outputDir, outputAuxFile = 'interfacesCreate_bucid.aux', id_field = 'BU_CID', dbstr = 'snowflakedb')        
    
    auxStr_list = []
    for x in runids:
       print(x)
       auxTmp = ''
       auxTmp = auxTmp + openCaseCopy(isoStr = args.isoStr, outputDir = outputDir, modelFile = modelFile, runid = x[0])
       auxTmp = auxTmp + close_transient
       auxTmp = auxTmp + close_additional
       auxTmp = auxTmp + inj_groups
       if x[0]>0:
           open_planned = generateAuxOpenPlannedOutages(outputDir, outputAuxFile = add_runid('plannedoutagesOpen.aux',x[0]), dateStr = x[1], isoStr = args.isoStr, dbstr = 'snowflakedb')
           auxTmp = auxTmp + open_planned
       auxTmp_1 = auxTmp #this version goint to have interfaces defined with names
       auxTmp_2 = auxTmp #this version goint to have interfaces defined with bucids
       auxTmp_1 = auxTmp_1 + interface_names
       auxTmp_2 = auxTmp_2 + interface_bucid
       auxTmp_1 = auxTmp_1 + saveCase(isoStr = args.isoStr, outputDir = outputDir, modelFile = modelFile, runid = x[0])
       auxTmp_2 = auxTmp_2 + saveCase(isoStr = args.isoStr, outputDir = outputDir, modelFile = modelFile, runid = x[0])
       #auxTmp_1 = auxTmp_1 + calc_tlrs 
       
       outputAuxFile = add_runid('assemble_model_interface_names.aux',x[0])
       outputAuxFileFullPath = fullPath(outputDir,outputAuxFile)

       auxStr = auxTmp_1
       with open(outputAuxFileFullPath, 'w') as file:
           file.write(auxStr)

       calc_tlrs = calcTLR(isoStr = args.isoStr, outputDir = outputDir, modelFile = modelFile, runid = x[0], outputAuxFile = add_runid('calctlrs.aux',x[0]))
        

    
