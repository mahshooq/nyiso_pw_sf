# -*- coding: utf-8 -*-
"""
Created on Wed Aug  14 12:53:12 2021

@author: Maxim.Naglis
"""

from collections import defaultdict
from functools import wraps
from functools import lru_cache as memoize
from functools import reduce
from operator import getitem
from time import time
import queue
from pandas import HDFStore
import warnings
import math
import os
import arrow
from tables import NaturalNameWarning
warnings.filterwarnings('ignore', category=NaturalNameWarning)

def add_timestamp(filename):

    pathname, extension = os.path.splitext(filename)
    ts = arrow.now().format('YYYYMMDD_HHmmss')
    return '{name}_{ts}{ext}'.format(name=pathname,ts=ts,ext=extension)
    
def distance(origin, destination):
    """
    Calculate the Haversine distance.

    Parameters
    ----------
    origin : tuple of float
        (lat, long)
    destination : tuple of float
        (lat, long)

    Returns
    -------
    distance_in_km : float

    Examples
    --------
    >>> origin = (48.1372, 11.5756)  # Munich
    >>> destination = (52.5186, 13.4083)  # Berlin
    >>> round(distance(origin, destination), 1)
    504.2
    """
    lat1, lon1 = origin
    lat2, lon2 = destination
    radius = 6371  # km

    dlat = math.radians(lat2 - lat1)
    dlon = math.radians(lon2 - lon1)
    a = (math.sin(dlat / 2) * math.sin(dlat / 2) +
         math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) *
         math.sin(dlon / 2) * math.sin(dlon / 2))
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    d = radius * c

    return d

def timing(f):
    @wraps(f)
    def wrap(*args, **kw):
        ts = time()
        result = f(*args, **kw)
        te = time()
        print ('func:%r args:[%r, %r] took: %2.4f sec' % (f.__name__, args, kw, te-ts))
        return result
    return wrap

def makehash():
    return defaultdict(makehash)

def manual_replace(s, char, index):
    return s[:index] + char + s[index +1:]
       
def get_nested_keys(d, keys, prefix = []):
    for k, v in d.items():
        if isinstance(v, dict):
            get_nested_keys(v, keys, prefix+[k])
        else:
            keys.append(prefix+[k])            

def get_nested_item(data, keys):
    try:
        return reduce(getitem, keys, data)
    except (KeyError, IndexError):
        return None

def set_nested_item(data, keys, value):
    for key in keys[:-1]:
        data = data.setdefault(key, makehash())
    data[keys[-1]] = value

def splitlist(l, n):
    """splits list l in n smaller lists"""
    return [lst[i:i + n] for i in range(0, len(lst), n)]

def list2queue(l, removeDups = True):
    """converts list to a queue"""    
    if removeDups: l = list(dict.fromkeys(l))
    q = queue.Queue()
    q.queue = queue.deque(l)
    return q

def storeMakehashToHDF5(makehashObj, makehashObjName, pathToStore = "./temp.hdf5", deleteStoreFirst= False):
    
    if deleteStoreFirst:
        try:
            import os
            os.remove(pathToStore)
        except:
            pass
        
    hdf = HDFStore(pathToStore,mode='a')
    
    key_list=[]
    get_nested_keys(makehashObj,key_list,[])

    count = 0
    for key in key_list:
        count = count + 1
        storekey = makehashObjName+'%'+"%".join(["'{0}'".format(k) if isinstance(k,str) else str(k) for k in key ])
        storekey = storekey.replace("'","")
        df = get_nested_item(makehashObj,key)
        print(count,key)
        try:
            
            hdf.append(key=storekey,value=df,format='table',data_columns=True)
        except:
            print("Error on {0}".format(storekey))
            hdf.close()
            raise
    print("hdf store keys:")
    storekeys = hdf.keys()
    for kk in storekeys:
        print(" ",kk)
    hdf.close()    
    return storekeys

def readMakehashFromHDF5(makehashObjName, pathToStore = "./temp.hdf5"):
    
    makehashObj = makehash()
    hdf = HDFStore(pathToStore,mode='r')
    keys = hdf.keys()
    for key in keys:
        keyParts = key.split("%")
        itemDictName = manual_replace(keyParts[0],'',0)
        itemKey = keyParts[1:]
        itemKey = [x.replace("'","") if isinstance(x,str) else x for x in itemKey]
        if makehashObjName == itemDictName:
            set_nested_item(makehashObj, itemKey, pd.read_hdf(hdf5file,key))
    hdf.close()
    return makehashObj 

if __name__ == '__main__':

    pass
