import arrow
import datetime
import pytz

def is_dst(zonename):
    tz = pytz.timezone(zonename)
    now = pytz.utc.localize(datetime.datetime.utcnow())
    return now.astimezone(tz).dst() != datetime.timedelta(0)

class dthelper(object):
    
    '''
    arrow based date time class
    '''

    def __init__(self,
                 dtstr = None, 
                 freq = 'H',
                 tz = None
                 ):
        
        if freq not in ['H','D','M']:
            raise ValueError("Only H, D and M frequencies are allowed!")
        else:
            self.__freq = freq

        if not dtstr:
            now = datetime.datetime.now()
            anow = arrow.get(now)
            if tz in ['C','c']:
                anow = anow.to('US/Central')
            elif tz in ['E','e']:
                anow = anow.to('US/Eastern')
            elif tz in ['P','p']:
                anow = anow.to('US/Pacific')
            elif tz in ['M','m']:
                anow = anow.to('US/Mountain')
            else:
                pass
            if self.__freq == 'M':
                self.__arrow = anow.replace(day=1)
            else:
                self.__arrow = anow
        else:
            if self.__freq == 'H':
                try:
                    self.__arrow = arrow.get(dtstr,'YYYY-MM-DD HH:mm')
                except:
                    raise ValueError("Hourly frequency. Datetime string must be in YYYY-MM-DD HH:mm format!")

            if self.__freq == 'D':
                try:
                    self.__arrow = arrow.get(dtstr,'YYYY-MM-DD')
                except:
                    raise ValueError("Daily frequency. Datetime string must be in YYYY-MM-DD format!")

            if self.__freq == 'M':
                try:
                    self.__arrow = arrow.get(dtstr,'YYYY-MM-DD').replace(day=1)
                except:
                    raise ValueError("Monthly frequency. Datetime string must be in YYYY-MM-DD format!")

    def freq(self):
        return self.__freq

    @property
    def dt(self):
        if self.__freq == 'H':
            return self.__arrow.datetime.replace(minute=0).replace(second=0).replace(microsecond=0).replace(tzinfo=None)
        if self.__freq == 'D':
            return self.__arrow.date()
        if self.__freq == 'M':
            return self.__arrow.replace(day=1).date()

        
    @dt.deleter
    def dt(self):
        raise Exception("Cannot delete dt's datetime!")
            
    @dt.setter
    def dt(self, value):
        if self.__freq == 'H':
            if isinstance(value,datetime.datetime):
                self.__arrow = arrow.get(value)
            else:
                raise ValueError("Only datetime.datetime object can be assigned to dthelper with H frequency!")
        if self.__freq == 'D':
            if isinstance(value,datetime.date):
                self.__arrow = arrow.get(value)
            else:
                raise ValueError("Only datetime.date object can be assigned to dthelper with D frequency!")
        if self.__freq == 'M':
            if isinstance(value,datetime.date):
                self.__arrow = arrow.get(value).replace(day=1)
            else:
                raise ValueError("Only datetime.date object can be assigned to dthelper with D frequency!")

            
    @property
    def date(self):
        return self.__arrow.date()

    @property
    def datestr(self):
        return self.__arrow.format('YYYY-MM-DD')

    @property
    def dtstr(self):
        if self.__freq == 'H':
            return self.__arrow.replace(minute=0).format('YYYY-MM-DD HH:mm')
        if self.__freq == 'D':
            return self.__arrow.format('YYYY-MM-DD')
        if self.__freq == 'M':
            return self.__arrow.format('YYYY-MM-DD')

        
    @dtstr.setter
    def dtstr(self, value):
        if self.__freq == 'H':
            try:
                fmt = 'YYYY-MM-DD HH:mm'
                self.__arrow = arrow.get(value,fmt)
            except:
                raise ValueError("Hourly frequency. Datetime string must be in YYYY-MM-DD HH:mm format!")
        if self.__freq == 'D':
            try:
                fmt = 'YYYY-MM-DD'
                self.__arrow = arrow.get(value,fmt)
            except:
                raise ValueError("Daily frequency. Dateteime string must be in YYYY-MM-DD format!")
        if self.__freq == 'M':
            try:
                fmt = 'YYYY-MM-DD'
                self.__arrow = arrow.get(value,fmt)
            except:
                raise ValueError("Monthly frequency. Dateteime string must be in YYYY-MM-DD format!")

    @dtstr.deleter
    def dtstr(self):
        raise Exception("Cannot delete datetime string!")

    def __add__(self, value) :
        if self.__freq == 'H':
            return dthelper(self.__arrow.shift(hours=value).format('YYYY-MM-DD HH:mm'),self.__freq)
        if self.__freq == 'D':
            return dthelper(self.__arrow.shift(days=value).format('YYYY-MM-DD'),self.__freq)
        if self.__freq == 'M':
            return dthelper(self.__arrow.shift(months=value).format('YYYY-MM-DD'),self.__freq)

    def __radd__(self, value) :
        if self.__freq == 'H':
            return dthelper(self.__arrow.shift(hours=value).format('YYYY-MM-DD HH:mm'),self.__freq)
        if self.__freq == 'D':
            return dthelper(self.__arrow.shift(days=value).format('YYYY-MM-DD'),self.__freq)
        if self.__freq == 'M':
            return dthelper(self.__arrow.shift(months=value).format('YYYY-MM-DD'),self.__freq)

    def __sub__(self, value) :
        if self.__freq == 'H':
            return dthelper(self.__arrow.shift(hours=-value).format('YYYY-MM-DD HH:mm'),self.__freq)
        if self.__freq == 'D':
            return dthelper(self.__arrow.shift(days=-value).format('YYYY-MM-DD'),self.__freq)
        if self.__freq == 'M':
            return dthelper(self.__arrow.shift(months=-value).format('YYYY-MM-DD'),self.__freq)

    def __rsub__(self, value) :
        if self.__freq == 'H':
            return dthelper(self.__arrow.shift(hours=-value).format('YYYY-MM-DD HH:mm'),self.__freq)
        if self.__freq == 'D':
            return dthelper(self.__arrow.shift(days=-value).format('YYYY-MM-DD'),self.__freq)
        if self.__freq == 'M':
            return dthelper(self.__arrow.shift(months=-value).format('YYYY-MM-DD'),self.__freq)

    def __eq__(self,other):
        if self.freq() != other.freq():
            raise TypeError('cannot compare dthelper objects with different frequencies')
        return self.dt == other.dt

    def __lt__(self,other):
        if self.freq() != other.freq():
            raise TypeError('cannot compare dthelper objects with different frequencies')
        return self.dt < other.dt

    def __le__(self,other):
        if self.freq() != other.freq():
            raise TypeError('cannot compare dthelper objects with different frequencies')
        return self.dt <= other.dt

    def __gt__(self,other):
        if self.freq() != other.freq():
            raise TypeError('cannot compare dthelper objects with different frequencies')
        return self.dt > other.dt

    def __ge__(self,other):
        if self.freq() != other.freq():
            raise TypeError('cannot compare dthelper objects with different frequencies')
        return self.dt >= other.dt

    def __ne__(self,other):
        if self.freq() != other.freq():
            raise TypeError('cannot compare dthelper objects with different frequencies')
        return self.dt != other.dt

    def __str__(self):

        dthelperinfo = "<{0} : {1}>"
        return dthelperinfo.format(self.__freq, self.dtstr)

    def __repr__(self):
        return self.__str__() 

    def __hash__(self):
        return hash((self.__freq, self.__arrow))
    
if __name__ == '__main__':

    a = dthelper()
