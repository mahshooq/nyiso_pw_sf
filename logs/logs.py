# -*- coding: utf-8 -*-
"""
Created on Wed Aug  16 08:39:18 2021

@author: Maxim.Naglis
"""

import os
import multiprocessing
import logging
from logging.handlers import QueueHandler, QueueListener

def logger_init(loggingLevel):
    
    m = multiprocessing.Manager()
    q = m.Queue()

    # this is the handler for all log records
    handler = logging.StreamHandler()
    handler.setFormatter(logging.Formatter("%(levelname)s: %(asctime)s - %(process)s - %(name)s - (%(filename)s).%(funcName)s line(%(lineno)d) - %(message)s"))

    # ql gets records from the queue and sends them to the handler
    ql = QueueListener(q, handler)
    ql.start()

    logger = logging.getLogger()
    logger.setLevel(loggingLevel)
    # add the handler to the logger so records from this process are handled
    logger.addHandler(handler)

    return ql, q

def worker(args):
    
    q, loggingLevel, n = args
    
    qh = QueueHandler(q)
    logger = logging.getLogger()
    logger.setLevel(loggingLevel)
    if logger.hasHandlers(): 
        logger.handlers = []
    logger.addHandler(qh)

    logging.info('hello from {0}'.format(os.getpid()))
    logging.debug('process {0}, got number {1}'.format(os.getpid(),n))
    
    print(n,os.getpid())
    return (n*2,os.getpid())

if __name__=='__main__':

    loggingLevel = logging.DEBUG   
 
    q_listener, q = logger_init(loggingLevel)
    logging.info('hello from main thread')
    
    nums = range(0,100)
    
    worker_args = []
    for n in nums:
        worker_args.append((q,loggingLevel,n))
    
    pool=multiprocessing.Pool(processes=multiprocessing.cpu_count())
    result=pool.map_async(worker,worker_args)
    pool.close()
    pool.join()
   
    q_listener.stop()
    
    print(result.get(timeout=1))    
