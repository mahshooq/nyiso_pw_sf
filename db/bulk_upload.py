import dask
import dask.dataframe as dd
import pathlib
import functools
from dask.diagnostics import ProgressBar
from snowflake.connector.pandas_tools import pd_writer
from pwsfx.db.db import create_snowflake_writer_engine

def csv_to_table_bulk_upload(
    csv_file_path: str = None,
    table_name: str = None,
    account_name: str = None,
    schema_name: str = None,
    database_name: str = None,
    warehouse_name: str = None,
    username: str = None,
    password: str = None,
    if_exists: str = 'fail',
    check_row_count: bool = False
) -> bool:
    try:
        if not csv_file_path:
            print('No csv_file_path specified')
            return
        if not pathlib.Path(f'{csv_file_path}').is_file():
            print('Given csv_file_path does not exists.')
            return
        if not table_name:
            print('No table_name specified.')
            return
        if not schema_name:
            print('No schema_name specified.')
            raise
        if not account_name:
            print('No account_name specified.')
            raise
        if not database_name:
            print('No database_name specified.')
            raise
        if not warehouse_name:
            print('No warehouse_name specified.')
            raise
        print(f'Using role OWNER_{database_name}')
        role_name = f'OWNER_{database_name}'
        if username and password:
            engine = create_snowflake_writer_engine(
                account = account_name,
                user = username,
                password = password,
                role = role_name,
                warehouse = warehouse_name,
                database = database_name,
                schema = schema_name
                )
        else:
            raise
        df = dd.read_csv(csv_file_path, blocksize='64MB')
        if check_row_count:
            num_of_rows_to_insert = len(df.shape[0])
            print(
                f'Uploading {csv_file_path} file with {num_of_rows_to_insert} rows to {database_name}.{schema_name}.{table_name}.')
        else:
            print(
                f'Uploading {csv_file_path} file to {database_name}.{schema_name}.{table_name}.')
        if not if_exists in ['fail', 'append', 'replace']:
            if_exists = 'fail'
        ddf_delayed = df.to_sql(
            table_name.lower(),
            uri=str(engine.url),
            schema=schema_name,
            if_exists=if_exists,
            index=False,
            method=functools.partial(pd_writer, quote_identifiers=False),
            compute=False,
            parallel=True
        )
        #with ProgressBar():
        dask.compute(ddf_delayed, scheduler='threads', retries=3)

        if check_row_count:
            print(
                f'Inserted {num_of_rows_to_insert} rows into table {database_name}.{schema_name}.{table_name}.')
        else:
            print(
                f'Inserted data into table {database_name}.{schema_name}.{table_name}.')
        return True
    except Exception as e:
        print(f"Caught an error. More Details : {e}")
        return False

def pandas_to_table_bulk_upload(
    pandasdf = None,
    table_name: str = None,
    account_name: str = None,
    schema_name: str = None,
    database_name: str = None,
    warehouse_name: str = None,
    username: str = None,
    password: str = None,
    if_exists: str = 'fail',
    check_row_count: bool = False
) -> bool:
    try:
        #if not csv_file_path:
        #    print('No csv_file_path specified')
        #    return
        #if not pathlib.Path(f'{csv_file_path}').is_file():
        #    print('Given csv_file_path does not exists.')
        #    return
        if type(pandasdf) == type(None):
            print('No pandas dataframe given')
            return
        if not table_name:
            print('No table_name specified.')
            return
        if not schema_name:
            print('No schema_name specified.')
            raise
        if not account_name:
            print('No account_name specified.')
            raise
        if not database_name:
            print('No database_name specified.')
            raise
        if not warehouse_name:
            print('No warehouse_name specified.')
            raise
        print(f'Using role OWNER_{database_name}')
        role_name = f'OWNER_{database_name}'
        if username and password:
            engine = create_snowflake_writer_engine(
                account = account_name,
                user = username,
                password = password,
                role = role_name,
                warehouse = warehouse_name,
                database = database_name,
                schema = schema_name
                )
        else:
            raise
        #df = dd.read_csv(csv_file_path, blocksize='64MB')
        df = dd.from_pandas(pandasdf, npartitions = 1000)
        if check_row_count:
            num_of_rows_to_insert = len(pandasdf.shape[0])
            print(
                f'Uploading dataframe with {num_of_rows_to_insert} rows to {database_name}.{schema_name}.{table_name}.')
        else:
            print(
                f'Uploading dataframe to {database_name}.{schema_name}.{table_name}.')
        if not if_exists in ['fail', 'append', 'replace']:
            if_exists = 'fail'
        ddf_delayed = df.to_sql(
            table_name.lower(),
            uri=str(engine.url),
            schema=schema_name,
            if_exists=if_exists,
            index=False,
            method=functools.partial(pd_writer, quote_identifiers=False),
            compute=False,
            parallel=True
        )
        with ProgressBar():
            dask.compute(ddf_delayed, scheduler='threads', retries=3)

        if check_row_count:
            print(
                f'Inserted {num_of_rows_to_insert} rows into table {database_name}.{schema_name}.{table_name}.')
        else:
            print(
                f'Inserted data into table {database_name}.{schema_name}.{table_name}.')
        return True
    except Exception as e:
        print(f"Caught an error. More Details : {e}")
        return False

    

if __name__ == "__main__":
    
    pass
