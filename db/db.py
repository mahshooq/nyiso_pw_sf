# -*- coding: utf-8 -*-
"""
Created on Wed Aug  4 22:43:18 2021

@author: Maxim.Naglis
"""

from sqlalchemy import create_engine
from snowflake.sqlalchemy import URL
import snowflake.connector

def execute_query(connection, query):
    
    print(query)
    cursor = connection.cursor()
    rs = cursor.execute(query)
    for row in rs:      
        print (row)
    cursor.close()    

def get_writer_conn(account,user,password,role,warehouse,database,schema):
    
    conn = snowflake.connector.connect(user=user, password = password, account = account, 
                                      warehouse = warehouse, database = database, schema=schema, role = role)    
    return conn    

def create_snowflake_writer_engine(account,user,password,role,
                                   warehouse,database,schema):
    engine = create_engine(URL(
        account=account,
        user=user,
        password=password,
        role=role,
        warehouse=warehouse,
        database=database,
        schema=schema
    ), echo=False)
    return engine

def create_snowflake_reader_engine(account,user,password,role,
                                   warehouse,database,schema):
    engine = create_engine(URL(
        account=account,
        user=user,
        password=password,
        role=role,
        warehouse=warehouse,
        database=database,
        schema=schema
    ), echo=False)
    return engine

def create_mysql_engine(user,password,host,port,db):
    
    connStrTmp = 'mysql://{user}:{password}@{host}:{port}/{db}'
    connStr = connStrTmp.format(user=user,
                                password=password,
                                host=host,
                                port=port,
                                db=db)
    #print(connStr)
    engine = create_engine(connStr)
    return engine 

def deleteSFXfromSF(db_conn, isoStr, startDate, endDate, runId, tableName = 'POWERDB_BIZDEV.PTEST.PWSFX_SFX_DAILY'):
        
    sqlStrTmp = '''
    DELETE FROM {tableName}
    WHERE BU_ISO = '{isoStr}' 
    AND FLOWMONTH =  '{startdate}' 
    AND RUNID = {runid}
    '''    
    
    sqlStr = sqlStrTmp.format(isoStr = isoStr, startdate = startDate, enddate = endDate, tableName = tableName, runid=runId)
    
    execute_query(db_conn, sqlStr)

    db_conn.close()    
    return 0

def saveSFXtoSF(db_conn, isoStr, startDate, endDate, runId, csvFile = None, tableName = 'POWERDB_BIZDEV.PTEST.PWSFX_SFX_DAILY', deleteFirst = True):
    
    if not csvFile:
        raise
    
    csvFile = csvFile.replace('''\\\\''','''\\''')
    csvFile = csvFile.replace('''\\''','''//''')    
    
    pathToFile = csvFile
    
    splitTableName = tableName.split('.')
    dataBaseName = splitTableName[0]
    schemaName = splitTableName[1]
    tableName = splitTableName[2]
    
    if dataBaseName != 'POWERDB_BIZDEV':
        raise
        
    sqlStr1 = 'USE DATABASE {0}'.format(dataBaseName)
    sqlStr2 = 'USE SCHEMA {0}'.format(schemaName)
    sqlStr3 = 'REMOVE @%{0}'.format(tableName)
    sqlStr4 = "PUT 'file://{0}' @%{1} overwrite=true".format(pathToFile,tableName)
    sqlStr5 = 'COPY INTO {0} file_format=(type=csv skip_header=1)'.format(tableName)
    
    queryList = [sqlStr1, sqlStr2, sqlStr3, sqlStr4, sqlStr5]
    
    for query in queryList:
        execute_query(db_conn, query)
        
    db_conn.close()
    return 0

if __name__ == '__main__':
    
    pass
    
