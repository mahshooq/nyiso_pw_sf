1) Setup db config file. Take example in the pwsfx config folder, modify as necessary, place in C:\Users\yourusername.
2) Setup app config file. Take example in the pwsfx config folder, modify as necessary, place in C:\Users\yourusername.
3) Fetch necessary files and place in the pwsfx input folder specified in the app config file.
4) Cleanup pwsfx output folder specified in the app config file as necessary (can create archive or tmp subfolder and move everythign there).
5) Run outagehelper. Currently table storing the planned outages is dropped and recreated when outagehelper is run. Results can be monitored with the following query:
SELECT ISO, OUTAGE_ISO, TICKETID, FACILITY_UID, TARGET_DATE, PLANNED_STARTDATE, PLANNED_ENDDATE, LASTCHANGEDATE, VOLTAGE, MONITORED_ELEMENT
FROM POWERDB_BIZDEV.PTEST.PWSFX_MISO_PLANNEDOUTAGES;
6) Run pwhelper. It will dump bus, gen, load, branch infrom from the FTR model files and write it to the snowflake, also it will parse model 
supplemental files and write info to the snowflake tables.
Currently tables storing model buses, loads, branches, gens, monitored elements, flowgates, nomograms, ems flowgates, 
schedulled outages, source and sinks, injection groups are dropped and recreated when pwhelper is run.
Currently in this file need to specify FTR model filename and also some supplemental files.
7) Run uochelper, it will update UoC for iso and create new set of records in the multiiso UoC for the period. 
8) Run auchelper, it will create aux files (assemble_model_interface_names_runidX.aux) for the base case and every day of the period to assemble model with correct
future topology, injection groups defined and interfaces defind. Also it will create aux files (calctlrs_runidX.aux) for the base case and every day of the period
to save assembled model, calculate TLR matrix and save TLR matrix to the CSV file. Currently in this file need to specify FTR model filename.
9) Run tlrhelper, it will run PowerWorld with calctlrs_runidX.aux file and produce CSV files with TLR matrix.
10) Run sfxhelper, it will transform wide TLR daily matrices to long SFX daily matrices and  upload them to the snowflake table.
12) Enjoy POWERDB_BIZDEV.PTEST.PWSFX_SFX_DAILY_VW and POWERDB_BIZDEV.PTEST.PWSFX_SFX_MONTHLY_VW.

