import sys
import traceback
import logging

class Error(Exception):
    '''Base class for exceptions'''
    pass

class TooManyRequests(Error):
    "Raised when we MU does not return data becasue it is overhelmed with our API calls"
    
class UnprocessableEntity(Error):
    "Raised when we MU gets unknown branch"    

class MonitoredBranchContingencyConflict(Error):
    "Raised when monitored line appears in he list of outaged branches"
    
class BadGateway(Error):
    "Sometimes MU throws wrenches like that"     
    
class RequestError(Error):
    "To catch errors on requests"     
    
class InternalServerError(Error):
    "To catch internal server error"    

if __name__ == '__main__':

    try:
        try:
            assert 2 == 3
        except:
            raise(Error)
    except:
        import sys, traceback
        exc_type, exc_value, exc_traceback = sys.exc_info()
        print("-"*60)
        print("*** print_tb:")
        traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
        print("*** print_exception:")
        traceback.print_exception(exc_type, exc_value, exc_traceback,
                                  limit=2, file=sys.stdout, chain=True)
        print("-"*60)


        
